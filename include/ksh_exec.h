////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

#ifndef KSH_EXEC_H
#define KSH_EXEC_H

/**
 * @file ksh_exec.h
 * @author Kiss Benedek
 * @brief Command execution engine function definitions
 */

#include "ksh_cmd.h"
#include "ksh_global.h"
#include <stdint.h>

//! Describes a standard UNIX pipe
union ksh_pipe {
    int raw[2];         //!< Raw data for pipe() syscall
    struct {
        int outfd;      //!< Output end of the pipe
        int infd;       //!< Input end of the pipe
        bool is_pipe;   //!< Indicates if this pipe was created via a pipe() syscall or not
    };
};

typedef union ksh_pipe ksh_pipe_t;


struct ksh_piop;

//! Pipe information for cross-subprocess piping (piop = Process Input/Output Pipes)
struct ksh_piop {
    ksh_pipe_t stdin;       //!< Pipe for stdin
    ksh_pipe_t stdout;      //!< Pipe for stdout
    ksh_pipe_t stderr;      //!< Pipe for stderr
    struct ksh_piop* prev;  //!< Pointer to the prevoious pipe configuration
};

typedef struct ksh_piop ksh_piop_t;


/**
 * @brief Converts command relations to string
 *
 * @return The string that matches the enum value
 * given as @p rel.
 */
const char* ksh_cmdrel_to_str(ksh_cmdrel_e rel);

/**
 * @brief Finds the executable for a command
 *
 * Searches for @p cmd in $PATH if cmd does not
 * contain path separators, otherwise tries
 * resolving the absolute path for it.
 *
 * @return The absoulute path to the executable
 * or #NULL if the command is unresolvable or
 * @p is #NULL.
 *
 * @note The caller is responsible for freeing
 * the returned pointer.
 */
char* ksh_resolve_command(const char* cmd);

/**
 * @brief Parses then executes every command in @p str
 *
 * Builds up a command chain from @p str, then
 * executes it according to the relations between
 * the commands.
 *
 * @note Arguments @p ksh and @p str cannot be null.
 *
 * @return true if the given string could be interpreted
 */
bool ksh_exec_s(ksh_t* ksh, const char* str);

/**
 * @brief Recusively executes the current_command form @p ksh
 *
 * @p pipes can be provided to redirect standard
 * file descriptors of the executed process.
 *
 * @note Argument @p ksh cannot be null.
 */
void ksh_exec(ksh_t* ksh, ksh_piop_t* pipes);

#endif
