////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_cmd.h
 * @author Kiss Benedek
 * @brief Objects releated to command parsing
 */

#ifndef KSH_CMD_H
#define KSH_CMD_H

#include "ksh_global.h"
#include "ksh_variable_list.h"

typedef enum {
    KSH_CMDREL_STANDALONE, //!< The command is independent
    KSH_CMDREL_AND,        //!< The command must be successful (exit code: 0) to execute the next one
    KSH_CMDREL_OR,         //!< The command must fail (exit code not 0) to execute the next one
    KSH_CMDREL_DETACH,     //!< The command should run asynchronously
    KSH_CMDREL_PIPE,       //!< The command's stdout is transfered to the next command's stdin
    KSH_CMDREL_FOUT,       //!< The command's stdout saved to a file
    KSH_CMDREL_FOUTA       //!< The command's stdout appended to a file
} ksh_cmdrel_e;


struct ksh_cmd_part;

//! Linked list for storing command arguments
struct ksh_cmd_part {
    char*                val;  //!< Raw string value
    struct ksh_cmd_part* next; //!< Pointer to the next entry
};

typedef struct ksh_cmd_part ksh_cmd_part_t;


struct ksh_cmd;

//! A type for describing builtin command handlers
typedef int (*ksh_builtin_f)(ksh_t*, const struct ksh_cmd*);

//! Stores information about a command chain
struct ksh_cmd {
    char*                   name;       //!< Name of the command, aka the program to be executed
    char*                   exe;        //!< The resolved full path of the program to be executed
    int16_t                 nargs;      //!< Number of command arguments
    int                     outfd;      //!< File descriptor for command output
    ksh_cmd_part_t*         args;       //!< Linked list storing the arguments
    ksh_cmd_part_t*         args_end;   //!< Last entry in the argument list
    ksh_cmdrel_e            rel;        //!< This commands's relation to the next one
    ksh_builtin_f           func;       //!< Function pointer for builtin handler
    ksh_variable_list_t*    vars;       //!< Environment variables for this command only
    struct ksh_cmd*         next;       //!< Next command in the command chain
};

typedef struct ksh_cmd ksh_cmd_t;


/**
 * @brief Creates a new ksh_cmd_t instance
 *
 * Allocates memory for a ksh_cmd_t then fills
 * it with default values. The command's relation
 * is set to #KSH_CMDREL_STANDALONE
 *
 * @return The created object
 */
ksh_cmd_t* ksh_cmd_init(ksh_cmdrel_e rel);

/**
 * @brief Destroys a ksh_cmd_t instance
 *
 * Frees up all memory used by a ksh_cmd_t instance,
 * then sets the referenced pointer to #NULL.
 */
void ksh_cmd_destroy(ksh_cmd_t** cmdref);

/**
 * @brief Appends a raw string to the command
 *
 * If the command has no name, it's set to @p part_str,
 * otherwise appended to the list of arguments.
 *
 * If @p alias_holder is set, resolves the commands
 * name from it
 */
void ksh_cmd_append_part(ksh_cmd_t* cmd, char* part_str, ksh_t* alias_holder);

/**
 * @brief Prints information stored in a ksh_cmd_t object
 *
 * Example:
 * @code
 * [D] ksh_cmd_t(ls) [
 *     ˝-l˝,
 *     ˝-a˝
 * ]
 * @endcode
 */
void ksh_cmd_debug_print(ksh_cmd_t* cmd);

/**
 * @brief Resolves variables in the given string
 *
 * Every matching pattern in @p part replaced whit
 * it's value. This function supports replacing
 * ${NAME} and $NAME variable formats.
 *
 * Regex objects are provided by @p ksh.
 *
 * @note Null and empty string are ignored.
 */
char* ksh_cmd_resolve_part(ksh_t* ksh, char* part);

/**
 * @brief Resolves all variables used in the given command object
 *
 * @see ksh_cmd_resolve_part
 */
void ksh_cmd_resolve_vars(ksh_t* ksh, ksh_cmd_t* cmd);

/**
 * @brief Executes a command
 *
 * Generates argument and environment variable list
 * then calls execve. This function will exit from
 * the application, must be used with fork()
 */
void ksh_exec_cmd(ksh_cmd_t* cmd, ksh_variable_list_t* env);

/**
 * @brief Gets left and right side of an equals sign in an argument
 *
 * Helper function used for local and global variable parsing. Splits
 * the given @p cmd_arg at the first equals sign, then sets the given
 * pointer references to buffers containing the sides.
 *
 * @return true if the parsing succeeded
 *
 * @note @p outleft and @p outright are only set, if the function
 * returned true, otherwise the pointers left intact.
 *
 * @bug Escape characters not handled correctly in every scenario
 */
bool ksh_split_equals_in_arg(const char* arg, char** outleft, char** outright);

#endif
