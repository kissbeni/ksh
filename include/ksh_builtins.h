////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_builtins.h
 * @author Kiss Benedek
 * @brief Functions for built-in commands
 */

#ifndef KSH_BUILTINS_H
#define KSH_BUILTINS_H

#include "ksh_global.h"
#include "ksh_cmd.h"

#define KSH_BUILTIN_HANDLER(x) int ksh_builtin__##x(ksh_t* ksh, const ksh_cmd_t* cmd)

//! builtin command handler for exit
KSH_BUILTIN_HANDLER(exit);

//! builtin command handler for cd
KSH_BUILTIN_HANDLER(cd);

//! builtin command handler for pushd
KSH_BUILTIN_HANDLER(pushd);

//! builtin command handler for popd
KSH_BUILTIN_HANDLER(popd);

//! builtin command handler for dirs
KSH_BUILTIN_HANDLER(dirs);

//! builtin command handler for alias
KSH_BUILTIN_HANDLER(alias);

//! builtin command handler for unalias
KSH_BUILTIN_HANDLER(unalias);

//! builtin command handler for export
KSH_BUILTIN_HANDLER(export);

//! builtin command handler for set
KSH_BUILTIN_HANDLER(set);

//! builtin command handler for unset
KSH_BUILTIN_HANDLER(unset);

#endif
