////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_variable_list.h
 * @author Kiss Benedek
 * @brief Structure and functions for variable lists
 */

#ifndef KSH_VARIABLE_LIST_H
#define KSH_VARIABLE_LIST_H

#include "ksh_global.h"

//! Linked list for storing local and gobal variables
struct ksh_variable_list {
    uint64_t key_hash;              //!< FNV64 hash of the variable name, used for fast searching
    char* key;                      //!< Name of the variable
    char* val;                      //!< Value of the variable
    struct ksh_variable_list* next; //!< Pointer to the next variable in the list
};

typedef struct ksh_variable_list ksh_variable_list_t;


//! Frees up a ksh_variable_list_t
void ksh_variable_list_free(ksh_variable_list_t** list);

/**
 * @brief Frees up a ksh_variable_list_t lightly
 *
 * Frees up only the list items, but keeps the memory
 * for keys and values intact.
 */
void ksh_variable_list_light_free(ksh_variable_list_t** list);

//! Sets a variable in a variable list
void ksh_variable_list_set(ksh_variable_list_t** list, char* name, char* value);

//! Removes a variable from a variable list
void ksh_variable_list_unset(ksh_variable_list_t** list, const char* name);

/**
 * @brief Merges two ksh_variable_lists together
 *
 * Creates a copy of a with a copy of b appended to the end of a,
 * without copying the strings in key and value to save memory and
 * time.
 *
 * Every matching key in a will be overwritten with the value in b.
 *
 * @return The merged list's first item
 *
 * @note You need to free the result with #ksh_variable_list_light_free
 */
ksh_variable_list_t* ksh_variable_list_merge(ksh_variable_list_t* a, ksh_variable_list_t* b);

//! Sets a local/global variable in the given @p ksh object
void ksh_set_variable(ksh_t* ksh, const char* name, char* value, bool is_local);

/**
 * @brief Resolves a local/global variable
 *
 * @return the value of the resolved variable, or NULL if
 * the variable could not be resolved.
 */
char* ksh_resolve_variable(ksh_t* ksh, const char* name, bool is_local);

//! Removes a variable with @p name
void ksh_unset_variable(ksh_t* ksh, const char* name, bool is_local);

#endif
