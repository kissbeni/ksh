////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_prompt.h
 * @author Kiss Benedek
 * @brief Structs and functions for user input and command history
 */

#ifndef KSH_PROMPT_H
#define KSH_PROMPT_H

#include <linux/limits.h>
#include <stdbool.h>
#include "ksh_global.h"

#ifndef KSH_USE_LIB_READLINE

    //! This values is used to indicate whether a ksh_history entry is loaded from file
    #define KSH_HISTFROMFILE_FLAG 0x80000000
    //! Checks the #KSH_HISTFROMFILE_FLAG in the given history_entry's id
    #define KSH_HISTID(x) ((x)->id & ~KSH_HISTFROMFILE_FLAG)

    #define ANSICB_CUU 'A' //!< ANSI control byte: Cursor up
    #define ANSICB_CUD 'B' //!< ANSI control byte: Cursor down
    #define ANSICB_CUF 'C' //!< ANSI control byte: Cursor forward
    #define ANSICB_CUB 'D' //!< ANSI control byte: Cursor back
    #define ANSICB_CNL 'E' //!< ANSI control byte: Cursor next line
    #define ANSICB_CPL 'F' //!< ANSI control byte: Cursor previous line
    #define ANSICB_CUE 'F' //!< ANSI control byte: Cursor to end
    #define ANSICB_CHA 'G' //!< ANSI control byte: Move cursor to column
    #define ANSICB_CUP 'H' //!< ANSI control byte: Move cursor to position
    #define ANSICB_CUH 'H' //!< ANSI control byte: Cursor to home
    #define ANSICB_ED  'J' //!< ANSI control byte: Erase display
    #define ANSICB_EL  'K' //!< ANSI control byte: Erase line
    #define ANSICB_SU  'S' //!< ANSI control byte: Scroll page up
    #define ANSICB_SD  'T' //!< ANSI control byte: Scroll page down
    #define ANSICB_HVP 'f' //!< ANSI control byte: Same as #ANSICB_CUP
    #define ANSICB_SGR 'm' //!< ANSI control byte: Change character apperance
    #define ANSICB_AUX 'i' //!< ANSI control byte: AUX Port control
    #define ANSICB_DSR 'n' //!< ANSI control byte: Device status report
    #define ANSICB_SCP 's' //!< ANSI control byte: Save cursor position
    #define ANSICB_RCP 'u' //!< ANSI control byte: Restore cursor position

    struct ksh_history;

    //! Stores information about a command history entry
    struct ksh_history {
        /**
         * @brief ID of this entry
         *
         * This field also contains information about
         * this entry is loaded from file or newly
         * added.
         *
         * @see KSH_HISTFROMFILE_FLAG
         */
        uint32_t id;
        char* line;                 //!< The history line
        struct ksh_history* prev;   //!< Pointer to the previous entry in the list
        struct ksh_history* next;   //!< Pointer to the next entry in the list
    };

    typedef struct ksh_history ksh_history_t;

#endif

//! Stores information about the command prompt
struct ksh_prompt {
    char user           [KSH_PROMPT_USER_MAXLEN]; //!< The current user's name
    char hostname       [KSH_PROMPT_HOST_MAXLEN]; //!< The computer's hostname
    char history_file   [PATH_MAX];               //!< Path for the history file
    char prompt_char;                             //!< The character idicating the privilege level

    #ifdef KSH_USE_LIB_READLINE
        int             last_hist_id;             //!< Last element index in history
    #else
        ksh_history_t* history;                   //!< Pointer to the last history entry
    #endif
};

typedef struct ksh_prompt ksh_prompt_t;


#ifndef KSH_USE_LIB_READLINE

    /**
     * @brief Stores information about an ANSI control sequence
     *
     * @see https://en.wikipedia.org/wiki/ANSI_escape_code
     * @see https://www.xfree86.org/4.8.0/ctlseqs.html
     */
    struct ksh_cs_params {
        int  int_params[5];   //!< Parsed integer params
        char separator_byte;  //!< The last character used to separate integers
        char final_byte;      //!< The last byte of the sequence
    };

    typedef struct ksh_cs_params ksh_cs_params_t;

#endif

/**
 * @brief Saves the current termios, then sets a new one
 *
 * In the new termios the echo and buffered i/o will be
 * disabled.
 *
 * You can restore the old termios with #restore_termios
 *
 * @see https://stackoverflow.com/a/7469410
 */
void save_and_set_termios();

/**
 * @brief Restores the stored termios
 *
 * Restores the original termios that was overwritten by
 * #save_and_set_termios()
 *
 * @see https://stackoverflow.com/a/7469410
 */
void restore_termios();

/**
 * @brief Reads a character from stdin withouth echoing it
 *
 * @see https://stackoverflow.com/a/7469410
 */
char getch();

#ifdef KSH_USE_LIB_READLINE
    /**
     * @brief Appends a line to history
     *
     * @p line is copied to a new buffer.
     * Null pointers are not allowed.
     */
    void ksh_prompt_append_history(ksh_prompt_t* prompt, char* line);
#else
    /**
     * @brief Creates a new ksh_history instance
     *
     * Allocates memory for a ksh_history_t, then fills
     * it with default values.
     *
     * @return The freshly allocated history object
     *
     * @note The caller is responsible for freeing up
     * the returned pointer.
     */
    ksh_history_t* ksh_prompt_history_init();

    /**
     * @brief Frees up every element in a history list
     *
     * Frees up all the memory used by the the history
     * entry referenced in @p historyref and every element
     * before and after it.
     *
     * The referenced pointer is set to #NULL.
     *
     * @note The referenced pointer _can_ be null, but
     * @p historyref itself cant.
     */
    void ksh_prompt_history_free(ksh_history_t** historyref);

    /**
     * @brief Appends a line to the given prompt's history
     *
     * If @p copy is true, @p line is copied to a new buffer
     * otherwise the given pointer is used. If the entry is
     * loaded form a file @p fromfile should be set to true;
     * this will add a flag to the id field in ksh_history_t.
     *
     * Null pointers are not allowed.
     */
    void ksh_prompt_append_history(ksh_prompt_t* prompt, char* line, bool copy, bool fromfile);

    /**
     * @brief Finds a history entry with the given id
     *
     * @return If there was a history entry with the given @id
     * a pointer to it, otherwise #NULL.
     */
    ksh_history_t* ksh_prompt_history_get(ksh_prompt_t* prompt, int32_t id);

    /**
     * @brief Writes the given prompt's history to file
     *
     * Every newly added history entry in @p prompt's
     * history is appended to the history file of the prompt.
     */
    void ksh_prompt_history_save(ksh_prompt_t* prompt);

    /**
     * @brief Prints a bash-style prompt
     *
     * Example:
     * @code
     * [user@host tmp]$
     * @endcode
     */
    void ksh_prompt_print(const ksh_t* prompt);

    /**
     * @brief Reads a command from stdin then passes it to the execution engine
     *
     * @return In case of EOF returns false, otherwise true.
     */
    bool ksh_prompt_read_and_exec(ksh_t* ksh);
#endif

/**
 * @brief Fills up the given prompt with initial values
 *
 * This function fills the following fields in the @p ksh's prompt:
 *  - hostname      with the system's hostname
 *  - user          with the current user's username
 *  - history_file  with the default history file path
 *  - prompt_char   according to the user's privilege level
 *
 * Loads the initial value on the directory stack
 *
 * Besides setting initial values, also loads the default
 * history file.
 */
void ksh_prompt_init(ksh_t* ksh);

//! Frees up memory used by variables inside the given @p prompt
void ksh_prompt_destroy(ksh_prompt_t* prompt);

/**
 * @brief Runs the prompt loop
 *
 * Reads and executes commands read from stdin,
 * exits on EOF or ksh->request_quit is set to true.
 */
void ksh_promt_read_loop(ksh_t* ksh);

#endif
