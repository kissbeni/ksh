////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_char_buffer.h
 * @author Kiss Benedek
 * @brief Functions and structures for ksh's character buffer
 */

#ifndef KSH_CHAR_BUFFER_H
#define KSH_CHAR_BUFFER_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

//! Checks if the given character is an UTF-8 slave
static inline bool is_utf8_s(char x) {
    return ((x & 0xC0) == 0x80);
}

//! Checks if the given character is a non UTF-8 (ASCII) character
static inline bool is_utf8_a(char x) {
    return (x & 0x7F) == x;
}

//! Checks if the given character is an UTF-8 master
static inline bool is_utf8_m(char x) {
    return is_utf8_a(x) || ((x & 0xC0) == 0xC0);
}

//! Counts non-zero bytes in the given 32 byte number
static inline size_t ksh_num_nz32(uint32_t x) {
    return !!(x & 0x000000FF) + !!(x & 0x0000FF00) + !!(x & 0x00FF0000) + !!(x & 0xFF000000);
}

//! Maximum numbers used by an UTF-8 character
#define KSH_CHAR_BUFFER_DATA_SIZE sizeof(uint32_t)

//! Default size of a ksh_char_buffer chunk size
#ifndef KSH_CHAR_BUFF_CHUNKSIZE
#define KSH_CHAR_BUFF_CHUNKSIZE 256 * KSH_CHAR_BUFFER_DATA_SIZE
#endif

//! Stores all information about a character buffer
struct ksh_char_buffer {
    uint32_t* data;         //!< Pointer to the allocated buffer
    size_t    data_size;    //!< Size of the allocated buffer
    int32_t   length;       //!< Number of characters stored
};

typedef struct ksh_char_buffer ksh_char_buffer_t;

/**
 * @brief Counts characters in the given strng
 *
 * Counts UTF-8 datapoints in the given zero-terminated
 * string.
 *
 * @return Number of UTF-8 characters in @p str
 *
 * @warning Do NOT use this for allocating buffers, unless you
 * know what are you doing!
 */
size_t utf8len(const char* str);

//! Gives how many bytes an UTF-8 character uses
uint8_t utf8_bytecount(char master);

/**
 * @brief Create a new instance
 *
 * Allocates a new ksh_char_buffer with an initial
 * chunk size defined with #KSH_CHAR_BUFF_CHUNKSIZE.
 *
 * @return A freshly allocated ksh_char_buffer instance
 */
ksh_char_buffer_t* ksh_char_buffer_init();

/**
 * @brief Resize the buffer
 *
 * Resizes the given buffer so it can fit at least @p newsize
 * UTF-8 characters. If @p newsize is smaller than the current
 * size the buffer's current size kept as is.
 */
void ksh_char_buffer_expand(ksh_char_buffer_t* buffer, size_t newsize);

/**
 * @brief Insert a character to the buffer
 *
 * Inserts the specified character at the given @p position,
 * or if the index is out of range, it's appended to the
 * end of the buffer.
 *
 * @return How much the buffer's length changed.
 */
int32_t ksh_char_buffer_insert(ksh_char_buffer_t* buffer, char what, int32_t pos);

/**
 * @brief Append a string to the buffer
 *
 * The given @p str is appended to the end of the buffer.
 * @warning @p str cannot be a null pointer
 */
void ksh_char_buffer_append_str(ksh_char_buffer_t* buffer, const char* str);

/**
 * @brief Remove a character from the buffer
 *
 * Removes an UTF-8 character from the given @p pos, or
 * if the index is out of range, removes the last character
 *
 * @return The number of bytes removed
 */
int ksh_char_buffer_remove(ksh_char_buffer_t* buffer, int32_t pos);

/**
 * @brief Print a memory dump of the buffer
 *
 * Prints a memory dump showing the raw bytes stored
 * in the allocated buffer. If @p mark is in the range
 * of it, it's underlined;
 */
void ksh_char_buffer_memdump(ksh_char_buffer_t* buffer, int32_t mark);

/**
 * @brief Get a character at a position
 *
 * @return The ASCII character at the given @p pos.
 *
 * @warning Out of bound indexing is not tolerated, and will cause an assertation failure!
 */
uint32_t ksh_char_buffer_at(ksh_char_buffer_t* buffer, int32_t pos);

/**
 * @brief Get the number of bytes stored in the buffer
 *
 * @return The number of bytes stored in the buffer
 */
int ksh_char_buffer_length(ksh_char_buffer_t* buffer);

/**
 * @brief Load a string into the buffer
 *
 * Resizes the buffer to be large enough to fit in @p str, then
 * the buffer's contents will be overwritten with it.
 */
void ksh_char_buffer_set_string(ksh_char_buffer_t* buffer, const char* str);

/**
 * @brief Make a copy of the buffer's contents
 *
 * @return A copy of the buffer starting from @p pos.
 *
 * @note The user is responsible for freeing up the returned pointer.
 */
char* ksh_char_buffer_make_string(ksh_char_buffer_t* buffer, int32_t pos);

/**
 * @brief Clears the buffer, but keeps the size of the allocated memory
 */
void ksh_char_buffer_clear(ksh_char_buffer_t* buffer);

/**
 * @brief Frees up the buffer
 *
 * Frees up the allocated memory then sets the given
 * reference to #NULL;
 */
void ksh_char_buffer_free(ksh_char_buffer_t** buffref);

#endif
