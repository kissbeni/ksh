////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_global.h
 * @author Kiss Benedek
 * @brief Global functions, macros, and structures
 */

#ifndef KSH_GLOBAL_H
#define KSH_GLOBAL_H

#include <stdbool.h>
#include <regex.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>


#define UNUSED1(z)          (void)(z)
#define UNUSED2(y,z)        UNUSED1(y),UNUSED1(z)
#define UNUSED3(x,y,z)      UNUSED1(x),UNUSED2(y,z)
#define UNUSED4(b,x,y,z)    UNUSED2(b,x),UNUSED2(y,z)
#define UNUSED5(a,b,x,y,z)  UNUSED2(a,b),UNUSED3(x,y,z)

#define UNUSED(x)           UNUSED1(x)

#define VA_NUM_ARGS_IMPL(_1,_2,_3,_4,_5, N,...) N
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 5, 4, 3, 2, 1)

#define ALL_UNUSED_IMPL_(nargs) UNUSED ## nargs
#define ALL_UNUSED_IMPL(nargs)  ALL_UNUSED_IMPL_(nargs)

/**
 * @brief Macro for supressing unused variable warnings
 *
 * @see https://stackoverflow.com/questions/23235910/variadic-unused-function-macro
 */
#define ALL_UNUSED(...) ALL_UNUSED_IMPL( VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__ )

#ifdef KSH_ALLOW_VERBOSE_PRINT

//! Conditional printf, used for debugging
#define DEBUG(...) { \
    if (modeflags & KSH_VM_VERBOSE) { \
        if (modeflags & KSH_VM_DEBUG_PIPE) \
            ksh_debug_pipe_printf(__VA_ARGS__); \
        else \
            printf(__VA_ARGS__); \
    } \
}

//! Conditional printf, used for memory debugging
#define MDEBUG(...) { \
    if (modeflags & KSH_VM_VERBOSE_MEM) { \
        if (modeflags & KSH_VM_DEBUG_PIPE) \
            ksh_debug_pipe_printf(__VA_ARGS__); \
        else \
            printf(__VA_ARGS__); \
    } \
}

#else
//! Conditional printf, used for debugging

#define DEBUG(...)  ALL_UNUSED(__VA_ARGS__)

//! Conditional printf, used for memory debugging
#define MDEBUG(...) ALL_UNUSED(__VA_ARGS__)

#endif


//! Indicates if an expression is likely to be true
#define likely(x)      __builtin_expect(!!(x), 1)

//! Indicates if an expression is unlikely to be true
#define unlikely(x)    __builtin_expect(!!(x), 0)


//! Assert with a printf call for formatted custom error messages
#define assertf(x, f, ...) if (unlikely(!(x))) { printf(f, __VA_ARGS__); assert(x); }

//! Assert with a puts call for custom error messages
#define assertp(x, f)      if (unlikely(!(x))) { puts(f); assert(x); }


//! Creates a string from the given arguemnt
#define STRINGIFY(x) #x

//! Macro for converting static stuff to string
#define TOSTRING(x) STRINGIFY(x)

#ifndef NULL
//! If NULL is not yet defined, let's make ours
#define NULL ((void*)0)
#endif

//! Maximum string length for username
#define KSH_PROMPT_USER_MAXLEN 64

//! Maximum string length for hostname
#define KSH_PROMPT_HOST_MAXLEN 64

//! Last exit code is set to this value if the command wasn't found
#define KSH_COMMAND_NOT_FOUND_EXITCODE 127

#define ASCII_ETX ((char)0x03)
#define ASCII_EOT ((char)0x04)
#define ASCII_TAB ((char)0x09)
#define ASCII_LF  ((char)0x0A)
#define ASCII_ESC ((char)0x1B)
#define ASCII_ETB ((char)0x17)
#define ASCII_CAN ((char)0x18)
#define ASCII_DEL ((char)0x7F)

//! Prime used for fnv64 hashing
#define FNV64_PRIME 0x100000001b3ULL

//! Initial hash value for fnv64
#define FNV64_INIT 0xcbf29ce484222325ULL


struct ksh_cmd;
struct ksh_prompt;
struct ksh_variable_list;

//! Verbose mode flags
enum ksh_verbose_mode {
    KSH_VM_NONE         = 0, //!< Default mode, prints no debug messages
    KSH_VM_VERBOSE      = 1, //!< Verbose mode, prints general debug messages
    KSH_VM_VERBOSE_MEM  = 2, //!< Verbose memory management mode, prints memory allocations and frees
    KSH_VM_DEBUG_PIPE   = 4  //!< Dumps everything through a FIFO pipe
};

typedef enum ksh_verbose_mode ksh_verbose_mode_e;


//! Linked list for the directory stack
struct ksh_directory_stack {
    char* dir;                          //!< Path to the directory
    struct ksh_directory_stack* prev;   //!< Pointer to the previous entry
};

typedef struct ksh_directory_stack ksh_directory_stack_t;


//! Linked list for storing aliases
struct ksh_alias_list {
    uint64_t command_hash;          //!< FNV64 hash of the command (used for searching)
    char* command;                  //!< The command to be replaced
    char** newparams;               //!< The new params to be addeded instead of the command
    struct ksh_alias_list* next;    //!< Pointer to the next entry
};

typedef struct ksh_alias_list ksh_alias_list_t;


/**
 * @brief The global ksh object
 *
 * This struct contains all the data used
 * by various ksh functions.
 */
typedef struct {
    struct ksh_cmd*             current_cmd;    //!< The current command object which is being executed
    struct ksh_prompt*          prompt;         //!< The main prompt object
    ksh_directory_stack_t*      dirstack;       //!< Top of the directory stack
    ksh_alias_list_t*           aliases;        //!< List of command aliases
    struct ksh_variable_list*   local_vars;     //!< List of variables in local context
    struct ksh_variable_list*   global_vars;    //!< List of variables in global context
    regex_t                     var_regex[2];   //!< Regular expressions for variable parsing
    int32_t                     last_exit_code; //!< The exit code of the most recently executed command
    bool                        request_quit;   //!< Request a graceful quit (exitcode will be set from last_exit_code)
    pid_t                       fork_pid;       //!< Process id of the child process which runs the command
} ksh_t;


//! Current verbose mode
ksh_verbose_mode_e modeflags;


/**
 * @brief ksh malloc wrapper
 *
 * Allocates a region on the heap with the given @p size,
 * and assigns a debug name to it.
 *
 * @return Pointer to the start of the region
 */
void* _malloc(size_t size, const char* debug_name);

/**
 * @brief ksh free wrapper
 *
 * Frees up the referenced memory region,
 * then sets it to a NULL pointer.
 * @p debug_name is used for debug and error
 * messages
 *
 * @note Null pointer references are tolerated,
 * null pointers are NOT!
 */
void _free(void** p, const char* debug_name);

/**
 * @brief ksh realloc wrapper
 *
 * Resizes the given memory region to a @p newsize.
 * If @p newsize is smaller than the current size
 * the original buffer is kept intact.
 *
 * @return A pointer to the start of the resized
 * area
 */
void* _realloc(void* p, size_t newsize);

/**
 * @brief Prints memory statistics
 *
 * This contains:
 *   - Total number of malloc/free/realloc calls
 *   - Total number of bytes allocated
 *   - Total number of bytes freed
 */
void ksh_print_mem_statistics();

/**
 * @brief Fancy way of allocating a new object
 *
 * Allocates memory for the given type, using #_malloc
 * with the type, filename and line number used as
 * debug name.
 */
#define new(x) (x*)_malloc(sizeof(x), #x"@"__FILE__":"TOSTRING(__LINE__))

/**
 * @brief Wrapper macro for #_free
 *
 * Frees up memory using #_free which sets the given pointer
 * to #NULL. The variable name, filename and the line number
 * passed as debug message.
 */
#define del(x) if (x) _free((void**)&x, #x"@"__FILE__":"TOSTRING(__LINE__))


/**
 * @brief Opens the debug FIFO pipe
 *
 * Opens a fifo pipe using the given @p fifo_path
 */
void ksh_open_debug_pipe(const char* fifo_path);

/**
 * @brief Closes the debug FIFO pipe
 */
void ksh_close_debug_pipe();

/**
 * @brief Prints a string to the debug FIFO pipe
 *
 * Works like puts
 */
void ksh_debug_pipe_puts(const char* s);

/**
 * @brief Prints a formatted string to the debug FIFO pipe
 *
 * Works like printf
 */
void ksh_debug_pipe_printf(const char* format, ...);

/**
 * @brief Enables the given verbose output mode
 */
static inline void ksh_enable_verbose_mode(ksh_verbose_mode_e mode) {
    modeflags |= mode;
}


/**
 * @brief Registers a new command alias
 *
 * Creates a new command alias with @p name if it doesn't already
 * exists in @p ksh.
 *
 * @return false if the alias already exists true otherwise
 */
bool ksh_add_alias(ksh_t* ksh, char* name, char** newcmd);

/**
 * @brief Removes a command alias
 *
 * Deletes the alias with the given @p name.
 *
 * @return false if the alias doesn't exist, true on success
 */
bool ksh_remove_alias(ksh_t* ksh, const char* name);

//! Frees up the alias list in the given @p ksh object
void ksh_free_alias_list(ksh_t* ksh);

/**
 * @brief Resolves an alias with the given @p name
 *
 * @return the parameters for the replaced command, or
 * NULL if the alias could not be resolved
 */
char** ksh_resolve_alias(ksh_t* ksh, const char* name);

//! Frees up the directory stack in the given @p ksh object
void ksh_free_directory_stack(ksh_t* ksh);


/**
 * @brief Gives the current working directory
 *
 * @return A freshly allocated buffer containing
 * the current working directory.
 *
 * @note The caller is responsible for freeing the
 * returned pointer
 */
char* get_mallocd_cwd();

/**
 * @brief Calculates the FNV64 hash of the given string
 *
 * Calculates a Fowler–Noll–Vo hash of @p str using
 * #FNV64_PRIME for the prime and #FNV64_INIT for the
 * initial hash value.
 *
 * @return The hash of @p str
 *
 * @see fnv64_strn
 */
uint64_t fnv64_str(const char* str);

/**
 * @brief Calculates the FNV64 hash of the given string with length
 *
 * Calculates a Fowler–Noll–Vo hash of @p str using
 * #FNV64_PRIME for the prime and #FNV64_INIT for the
 * initial hash value.
 *
 * The @p length of the string can be specified, allowing hasing
 * strings containing zero-terminators, if it's set to -1 the
 * string is processed until the first zero byte.
 *
 * @return The hash of @p str
 *
 * @see https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
 */
uint64_t fnv64_strn(const char* str, size_t length);

#endif
