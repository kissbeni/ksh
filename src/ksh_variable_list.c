////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_variable_list.c
 * @author Kiss Benedek
 * @brief Implementation of functions for variable lists
 */

#include "ksh_global.h"
#include "ksh_variable_list.h"

#include <string.h>

void ksh_variable_list_free(ksh_variable_list_t** list) {
    while (*list) {
        ksh_variable_list_t* next = (*list)->next;
        del((*list)->key);
        del((*list)->val);
        del(*list);
        *list = next;
    }
}

void ksh_variable_list_light_free(ksh_variable_list_t** list) {
    while (*list) {
        ksh_variable_list_t* next = (*list)->next;
        del(list);
        *list = next;
    }
}

void ksh_variable_list_set(ksh_variable_list_t** list, char* name, char* value) {
    uint64_t hash = fnv64_str(name);

    DEBUG("[D] Setting variable '%s' [0x%016lx] to '%s'\n", name, hash, value);

    for (ksh_variable_list_t* item = *list; item; item = item->next)
        if (item->key_hash == hash) {
            del(item->val);
            del(name);
            item->val = value;
            return;
        }

    ksh_variable_list_t* ni = new(ksh_variable_list_t);
    ni->next = *list;
    ni->key = name;
    ni->key_hash = hash;
    ni->val = value;

    *list = ni;
}

void ksh_variable_list_unset(ksh_variable_list_t** list, const char* name) {
    uint64_t hash = fnv64_str(name);

    for (ksh_variable_list_t *item = *list, *last = NULL; item; last = item, item = item->next)
        if (item->key_hash == hash) {
            if (last)
                last->next = item->next;
            else
                *list = item->next;

            del(item->key);
            del(item->val);
            del(item);
            return;
        }
}

ksh_variable_list_t* ksh_variable_list_merge(ksh_variable_list_t* a, ksh_variable_list_t* b) {
    ksh_variable_list_t* res = NULL;

    for (;a;a = a->next)
        ksh_variable_list_set(&res, a->key, a->val);

    for (;b;b = b->next)
        ksh_variable_list_set(&res, b->key, b->val);

    return res;
}

void ksh_set_variable(ksh_t* ksh, const char* name, char* value, bool is_local) {
    ksh_variable_list_t** list = (is_local) ? &ksh->local_vars : &ksh->global_vars;

    char* _name = _malloc(strlen(name) + 1, "ksh_variable_list_set$var_name");
    strcpy(_name, name);

    ksh_variable_list_set(list, _name, value);
}

char* ksh_resolve_variable(ksh_t* ksh, const char* name, bool is_local) {
    ksh_variable_list_t* list = (is_local) ? ksh->local_vars : ksh->global_vars;
    uint64_t hash = fnv64_str(name);

    for (ksh_variable_list_t* item = list; item; item = item->next)
        if (item->key_hash == hash)
            return item->val;

    return NULL;
}

void ksh_unset_variable(ksh_t* ksh, const char* name, bool is_local) {
    ksh_variable_list_t** list = (is_local) ? &ksh->local_vars : &ksh->global_vars;
    ksh_variable_list_unset(list, name);
}
