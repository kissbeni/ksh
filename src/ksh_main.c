////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_main.c
 * @author Kiss Benedek
 * @brief The entry point of the application
 */

#include "ksh_prompt.h"
#include "ksh_exec.h"

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <termios.h>
#include <getopt.h>
#ifdef KSH_USE_LIB_READLINE
#  include <readline/readline.h>
#  include <readline/history.h>
#endif
#include "version.h"

const char* ksh_known_shell_history_files[] = {
    "/.zsh_history",
    "/.bash_history",
    "/.ksh_history",  // Useful if the user specifies an other history file
    NULL
};

//! Variable for the global ksh_t object (used by signal handler)
static ksh_t ksh;

//! Path for the history file to import
static char* histfile_to_import = NULL;

//! The command specified with -c witch
static char* cmd_to_exec = NULL;

/**
 * @brief Gracefully closes program
 *
 * Appends history to the histfile, then
 * frees up all used memory, finally exits
 * the program with the given @p exit_code
 */
void ksh_quit(int exit_code) {
    ksh_prompt_destroy(ksh.prompt);

    ksh_free_alias_list(&ksh);
    ksh_free_directory_stack(&ksh);
    ksh_variable_list_free(&ksh.local_vars);
    ksh_variable_list_free(&ksh.global_vars);

    regfree(&ksh.var_regex[0]);
    regfree(&ksh.var_regex[1]);

    ksh_print_mem_statistics();
    DEBUG("---------- process close ----------\n");

    exit(exit_code);
}

//! Handler for kill signals
void ksh_handle_signal(int sig) {
    DEBUG("[D] got signal: %d fork_pid=0x%04x\n", sig, ksh.fork_pid);

    if (ksh.fork_pid != 0) {
        kill(ksh.fork_pid, sig);
    } else if (sig == SIGINT) {
        #ifdef KSH_USE_LIB_READLINE
            puts("");
            rl_on_new_line();
            rl_replace_line("", 0);
            rl_redisplay();
        #else
            char c = ASCII_CAN;
            ioctl(STDIN_FILENO, TIOCSTI, &c);
        #endif
    }
}

//! Command line argument processor
bool ksh_process_args(int argc, char *argv[]) {
    static struct option long_options[] = {
        {"help",       no_argument, 0, 'h'},
        {"verbose",    no_argument, 0, 'v'},
        {"memdebug",   no_argument, 0, 'V'},
        {"debug-pipe", no_argument, 0, 'p'},
        {0, 0, 0, 0}
    };

    int option_index = 0;
    char opt;
    while ((opt = getopt_long(argc, argv, "hc:H:i:vVp", long_options, &option_index)) != -1) {
        switch (opt) {
            case 0:
                break;

            case 'h':
                printf("ksh version %s\n", KSH_VERSION);
                printf("Usage: %s [options]\n", argv[0]);
                puts("");
                puts("Options:");
                puts(" -h, --help            Prints this help");
                puts(" -c COMMAND            Executes the given command then quit");
                puts(" -H HISTFILE           Specify the path of the history file");
                puts(" -i HISTFILE           Import to history from the given file");

                #ifdef KSH_ALLOW_VERBOSE_PRINT
                    puts(" -v, --verbose         Be verbose");

                    #ifdef KSH_MEMORY_DEBUGGING
                        puts(" -V, --memdebug        Debug dynamic memory handling");
                    #endif

                    puts(" -p, --debug-pipe      Opens a FIFO pipe at /tmp/ksh_debug for debugging");
                #endif

                puts("");
                puts("Builtin commands:");
                puts(" exit                 Exit from shell");
                puts(" cd                   Change directory");
                puts(" chdir                Same as cd");
                puts(" pushd                Push a path to the directory stack");
                puts(" popd                 Pop a directory from the directory stack");
                puts(" dirs                 Print the directory stack");
                return false;

            case 'c':
                cmd_to_exec = _malloc(strlen(optarg) + 1, "exec_cmdname");
                strcpy(cmd_to_exec, optarg);
                return true;

            case 'H':
                strncpy(ksh.prompt->history_file, optarg, PATH_MAX - 1);
                return true;

            case 'i':
                histfile_to_import = _malloc(strlen(optarg) + 1, "import_histfile");
                strcpy(histfile_to_import, optarg);
                return true;

            case 'v':
                ksh_enable_verbose_mode(KSH_VM_VERBOSE);
                break;

            case 'V':
                ksh_enable_verbose_mode(KSH_VM_VERBOSE_MEM);
                break;

            case 'p':
                ksh_enable_verbose_mode(KSH_VM_DEBUG_PIPE);
                break;

            case '?':
                return false;

            default:
                abort();
        }
    }

    return true;
}

/**
 * @brief Imports history from a @p file into the given @p ksh's history
 *
 * @return true if the import was completed successfully
 */
bool ksh_import_history(ksh_t* ksh, const char* file) {
    if (!file)
        return false;

    printf("Importing history from %s\n", file);
    FILE* fin = fopen(file, "rt");

    if (fin == NULL) {
        printf("Could not import history from %s: could not open file\n", file);
        return false;
    }

    int n_imported = 0;

    char* line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, fin)) != -1) {
        line[read - 1] = 0;

        #ifndef KSH_USE_LIB_READLINE
            ksh_prompt_append_history(ksh->prompt, line, true, false);
        #else
            ksh_prompt_append_history(ksh->prompt, line);
        #endif

        n_imported++;
    }

    free(line); // do not use our custom free here!

    printf("Imported %d lines of history\n", n_imported);

    fclose(fin);
    return true;
}

//! Reads an answer for a y/n question, with a default option
bool ksh_ask(bool def) {
    char ch;
    while (((ch = getch()) != EOF) && ch != 'y' && ch != 'Y' && ch != 'n' && ch != 'N' && ch != '\n')
        if (ch >= 0x20 && ch < 0x7f)
            printf("%c\x1b[D", ch);

    putchar(ch);

    if (ch == '\n')
        return def;

    putchar('\n');

    return (ch == 'y' || ch == 'Y');
}

//! If the history file does not exist, asks the user to import one
void ksh_ask_and_import(ksh_t* ksh) {
    struct stat st;

    if (stat(ksh->prompt->history_file, &st) == 0) {
        DEBUG("[D] Ask&import: history file exists, there is nothing to do\n");
        return;
    }

    char tmp[PATH_MAX];
    int homepos = 0;
    strncpy(tmp, getenv("HOME"), PATH_MAX - 1);
    homepos = strlen(tmp);

    const char** paths = ksh_known_shell_history_files;
    while (*paths) {
        tmp[homepos] = 0;
        strcat(tmp, *paths);

        if (stat(tmp, &st) == 0) {
            printf("Found history file '%s' do you want to import it? [Y/n]: ", tmp);

            if (ksh_ask(true))
                ksh_import_history(ksh, tmp);
        }

        paths++;
    }
}

//! Entry point of the appication
int main(int argc, char *argv[]) {
    ksh_prompt_t prompt;
    prompt.history_file[0] = 0;
    modeflags = KSH_VM_NONE;

    ksh.last_exit_code = 0;
    ksh.current_cmd = NULL;
    ksh.prompt = &prompt;
    ksh.fork_pid = 0;
    ksh.request_quit = false;
    ksh.aliases = NULL;
    ksh.local_vars = NULL;
    ksh.global_vars = NULL;

    if (!ksh_process_args(argc, argv))
        return 1;

    DEBUG("---------- process start ----------\n");

    ksh.dirstack = new(ksh_directory_stack_t);
    ksh.dirstack->dir = get_mallocd_cwd();
    ksh.dirstack->prev = NULL;

    if (regcomp(&ksh.var_regex[0], "\\$([A-Za-z\?_][A-Za-z0-9_]*)", REG_EXTENDED)) {
        puts("ksh: failed to create prompt regex 0");
        return 1;
    }

    if (regcomp(&ksh.var_regex[1], "\\$\\{([A-Za-z_][A-Za-z0-9_]*)\\}", REG_EXTENDED)) {
        regfree(&ksh.var_regex[0]);
        puts("ksh: failed to create prompt regex 1");
        return 1;
    }

    signal(SIGINT, ksh_handle_signal);
    signal(SIGTERM, ksh_handle_signal);

    if (cmd_to_exec) {
        ksh_exec_s(&ksh, cmd_to_exec);
        del(cmd_to_exec);
        ksh_quit(ksh.last_exit_code);
    }

    ksh_prompt_init(&ksh);

    if (histfile_to_import) {
        bool res = ksh_import_history(&ksh, histfile_to_import);
        del(histfile_to_import);

        if (!res) {
            ksh_quit(1);
            return 1;
        }
    }

    ksh_ask_and_import(&ksh);
    ksh_promt_read_loop(&ksh);

    if (!ksh.request_quit)
        putchar('\n');

    ksh_quit(ksh.last_exit_code);
    return ksh.last_exit_code;
}
