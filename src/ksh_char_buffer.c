////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_char_buffer.c
 * @author Kiss Benedek
 * @brief Function implementations for ksh's character buffer
 */

#include "ksh_char_buffer.h"
#include "ksh_global.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

uint8_t utf8_bytecount(char master) {
    if (is_utf8_a(master))
        return 1;

    uint8_t slaves;

    for (slaves = 0; master & 0x80; slaves++)
        master <<= 1;

    return slaves;
}

/**
 * @brief Checks if the given @p dataset contains as many bytes as
 * indicated in the master byte
 */
bool utf8_full(uint32_t dataset) {
    return ksh_num_nz32(dataset) == utf8_bytecount(dataset);
}

size_t utf8len(const char* str) {
    size_t res = 0;
    for (size_t i = 0; str[i] != '\0'; i++)
        if (is_utf8_m(str[i]))
            res++;

    return res;
}

ksh_char_buffer_t* ksh_char_buffer_init() {
    ksh_char_buffer_t* buff = new(ksh_char_buffer_t);
    buff->data_size = KSH_CHAR_BUFF_CHUNKSIZE;
    buff->data = _malloc(KSH_CHAR_BUFF_CHUNKSIZE, "char_buffer->data");
    buff->data[0] = 0;
    buff->length = 0;
    return buff;
}

void ksh_char_buffer_expand(ksh_char_buffer_t* buff, size_t newlen) {
    newlen *= KSH_CHAR_BUFFER_DATA_SIZE;

    if (newlen < buff->data_size)
        return;

    DEBUG("[D] ksh_char_buffer_expand newlen=%ld current_length=%d data_size=%ld\n",
        newlen, buff->length, buff->data_size);

    size_t extra_size = ((newlen / KSH_CHAR_BUFF_CHUNKSIZE + 1)) *
        KSH_CHAR_BUFF_CHUNKSIZE - buff->data_size;

    DEBUG("[D] Extra size: %ld\n", extra_size);

    buff->data_size += extra_size;
    buff->data = _realloc(buff->data, buff->data_size);
}

int32_t ksh_char_buffer_insert(ksh_char_buffer_t* buff, char ch, int32_t pos) {
    ksh_char_buffer_expand(buff, buff->length + 1);

    size_t length_before = buff->length;
    uint8_t uch = (uint8_t)ch; // make the character unsigned

    // insert at specific place
    if (unlikely(buff->length > 0 && (pos >= 0 && pos < buff->length))) {

        if (pos > 0 && !utf8_full(buff->data[pos - 1]))
            pos--;

        // UTF-8 master character or the selected character is full
        if (is_utf8_m(ch) || utf8_full(buff->data[pos])) {
            memcpy(
                buff->data + pos + 1,
                buff->data + pos,
                (buff->length - pos) * KSH_CHAR_BUFFER_DATA_SIZE
            );

            buff->data[pos] = uch;
            buff->length++;
        } else {
            uint8_t nb = ksh_num_nz32(buff->data[pos]) * 8;
            buff->data[pos] |= (uint32_t)uch << nb;
        }
    } else { // append
        // UTF-8 master character or the selected character is full
        if (is_utf8_m(ch) || utf8_full(buff->data[buff->length - 1])) {
            buff->data[buff->length] = uch;
            buff->length++;
        } else {
            uint8_t nb = ksh_num_nz32(buff->data[buff->length - 1]) * 8;
            buff->data[buff->length - 1] |= (uint32_t)uch << nb;
        }
    }

    buff->data[buff->length] = 0;
    return buff->length - length_before;
}

void ksh_char_buffer_append_str(ksh_char_buffer_t* buff, const char* str) {
    assertp(str, "[!] ksh_char_buffer_append_str got nullptr!");

    ksh_char_buffer_expand(buff, buff->length + utf8len(str));

    size_t slen = strlen(str);
    for (size_t si = 0; si < slen; si++)
        ksh_char_buffer_insert(buff, str[si], -1);
}

int32_t ksh_char_buffer_remove(ksh_char_buffer_t* buff, int32_t pos) {
    int32_t res = 1;

    // remove at specific place
    if (pos >= 0 && pos <= buff->length) {
        for (int32_t i = pos; i < buff->length; i++)
            buff->data[i] = buff->data[i + 1];

        buff->data[buff->length - 1] = 0;
    } else { // remove last
        buff->data[buff->length - 1] = 0;
    }

    buff->length--;
    return res;
}

void ksh_char_buffer_memdump(ksh_char_buffer_t* buff, int32_t markpos) {
    bool reached_zero = false;
    bool reached_zero2 = false;

    for (size_t i = 0; i < buff->data_size / 16; i += 16) {
        DEBUG("%08lx: ", i);

        reached_zero2 = reached_zero;
        for (size_t j = i; j < i + 16; j++) {
            if (j >= buff->data_size) {
                DEBUG("???????? ");
            } else {
                if (markpos == j) {
                    DEBUG("\e[42m%08x\e[49m ", buff->data[j]);
                } else if (reached_zero || reached_zero2) {
                    DEBUG("\e[2m%08x\e[22m ", buff->data[j]);
                } else if (buff->data[j] == 0) {
                    DEBUG("\e[41m%08x\e[49m ", buff->data[j]);
                    reached_zero2 = true;
                } else {
                    DEBUG("%08x ", buff->data[j]);
                }
            }
        }

        DEBUG("| ");

        for (size_t j = i; j < i + 16; j++)
            if (j >= buff->data_size) {
                DEBUG(" ");
            } else {
                if (markpos == j) {
                    DEBUG("\e[42m%.4s\e[49m", (char*)&buff->data[j]);
                } else if (reached_zero) {
                    DEBUG("\e[2m.\e[22m");
                } else if (buff->data[j] == 0) {
                    DEBUG("\e[41m%.4s\e[49m", (char*)&buff->data[j]);
                    reached_zero = true;
                } else {
                    DEBUG("%.4s", (char*)&buff->data[j]);
                }
            }

        DEBUG(" |\n");
    }
}

uint32_t ksh_char_buffer_at(ksh_char_buffer_t* buff, int32_t pos) {
    if (pos < 0)
        pos = buff->length + pos;

    assertf((pos > 0 && pos < buff->length),
        "[!] char_buffer indexed out of range (pos=%d)\n", pos);

    return buff->data[pos];
}

void ksh_char_buffer_set_string(ksh_char_buffer_t* buff, const char* data) {
    ksh_char_buffer_clear(buff);
    ksh_char_buffer_expand(buff, utf8len(data));

    size_t slen = strlen(data);
    for (size_t si = 0; si < slen; si++)
        ksh_char_buffer_insert(buff, data[si], -1);
}

char* ksh_char_buffer_make_string(ksh_char_buffer_t* buff, int32_t start_pos) {
    if (start_pos < 0 || start_pos > buff->length)
        return NULL;

    size_t numchars = 0;
    for (int32_t li = start_pos; li < buff->length; li++)
        numchars += ksh_num_nz32(buff->data[li]);

    char* res = _malloc(numchars + 5, "ksh_char_buffer_make_string");
    size_t respos = 0;

    for (int32_t li = start_pos; li < buff->length; li++) {
        *((uint32_t*)&res[respos]) = buff->data[li];
        respos += ksh_num_nz32(buff->data[li]);
    }

    res[numchars] = 0;
    return res;
}

void  ksh_char_buffer_clear(ksh_char_buffer_t* buff) {
    buff->length = 0;
    buff->data[0] = 0;
}

void ksh_char_buffer_free(ksh_char_buffer_t** buff) {
    ksh_char_buffer_t* _buff = *buff;

    del(_buff->data);
    del(_buff);
    *buff = NULL;
}
