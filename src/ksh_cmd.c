////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_cmd.c
 * @author Kiss Benedek
 * @brief Command processing releated functions
 */

#include "ksh_global.h"
#include "ksh_cmd.h"
#include "ksh_char_buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h>
#include <string.h>

/**
 * @brief Replaces a part of a string
 *
 * @return A reallocated string with the given part replaced
 */
char* ksh_str_part_replace(char* buff, char* begin, int32_t lenf, char* target) {
    int32_t lenb = strlen(buff), lent = 0;

    if (target && target[0])
        lent = strlen(target);

    DEBUG("[D] Var before replacing [%d] '%*.s' in '%s'\n", lenf, lenf, begin, buff);

    char* new_s = _malloc(lenb - lenf + lent + 1, "ksh_str_part_replace$new_s");

    new_s[0] = 0;
    if (begin != buff) {
        strncpy(new_s, buff, begin - buff);

        // strncpy does not terminate the string
        new_s[begin - buff] = 0;
    }

    if (target)
        strcat(new_s, target);

    strcat(new_s, begin + lenf);
    DEBUG("[D] Var after  replacing [%d] '%*.s' in '%s'\n", lenf, lenf, begin, buff);
    DEBUG("[D] Var replace: '%.*s' => '%s'\n", lenf, begin, target ? target : "");
    del(buff);
    return new_s;
}

ksh_cmd_t* ksh_cmd_init(ksh_cmdrel_e rel) {
    ksh_cmd_t* res = new(ksh_cmd_t);
    memset(res, 0, sizeof(ksh_cmd_t));

    res->nargs = 0;
    res->rel = rel;
    res->outfd = STDOUT_FILENO;

    return res;
}

void ksh_cmd_destroy(ksh_cmd_t** cmd) {
    if (!cmd || !*cmd)
        return;

    ksh_cmd_t* c = *cmd;
    del(c->name);

    ksh_cmd_part_t* p;
    while (c->args) {
        p = c->args->next;
        del(c->args->val);
        del(c->args);
        c->args = p;
    }

    c->args_end = NULL;
    ksh_cmd_destroy(&c->next);
    ksh_variable_list_free(&c->vars);
    del(*cmd);
}

void ksh_cmd_append_part(ksh_cmd_t* cmd, char* val, ksh_t* alias_holder) {
    if (!cmd->name) {
        char *left, *right;
        if (ksh_split_equals_in_arg(val, &left, &right)) {
            ksh_variable_list_set(&cmd->vars, left, right);
            del(val);
            return;
        }

        if (alias_holder) {
            char** alias = ksh_resolve_alias(alias_holder, val);
            if (alias) {
                if (strcmp(*alias, val) == 0)
                    alias_holder = NULL;

                del(val);

                while (*alias) {
                    char* partval = _malloc(strlen(*alias) + 1, "alias_cmd_parm");
                    strcpy(partval, *alias);
                    ksh_cmd_append_part(cmd, partval, alias_holder);
                    alias++;
                }

                return;
            }
        }

        cmd->name = val;
        return;
    }

    ksh_cmd_part_t* new_part = new(ksh_cmd_part_t);
    new_part->val = val;
    new_part->next = NULL;

    if (!cmd->args)
        cmd->args = new_part;
    else
        cmd->args_end->next = new_part;

    cmd->args_end = new_part;
    cmd->nargs++;
}

void ksh_cmd_debug_print(ksh_cmd_t* cmd) {
    DEBUG("[D] ksh_cmd (%s) [\n", cmd->name);

    ksh_cmd_part_t* p = cmd->args;
    while (p) {
        DEBUG("        ˝%s˝", p->val);

        if (p->next)
            DEBUG(",");

        DEBUG("\n");
        p = p->next;
    }

    DEBUG("    ]\n");
}

char* ksh_cmd_resolve_part(ksh_t* ksh, char* in) {
    // there is nothing to replace in null or empty
    // strings
    if (in && in[0]) {
        regmatch_t regg[3];
        while (!regexec(&ksh->var_regex[0], in, 3, regg, 0) ||
               !regexec(&ksh->var_regex[1], in, 3, regg, 0)) {

            regoff_t len = regg[1].rm_eo - regg[1].rm_so;
            char* tmp = _malloc(regg[0].rm_eo - regg[0].rm_so + 1, "ksh_cmd_resolve_part");
            strncpy(tmp, in + regg[1].rm_so, len);
            tmp[len] = 0;

            DEBUG("[D] Resolving variable: %s\n", tmp);
            char* val = ksh_resolve_variable(ksh, tmp, true);

            if (!val && !(val = ksh_resolve_variable(ksh, tmp, false)))
                val = getenv(tmp);

            len = regg[0].rm_eo - regg[0].rm_so;
            strncpy(tmp, in + regg[0].rm_so, len);

            in = ksh_str_part_replace(in, in + regg[0].rm_so, len, val);
            del(tmp);
        }
    }

    return in;
}

void ksh_cmd_resolve_vars(ksh_t* ksh, ksh_cmd_t* cmd) {
    DEBUG("[D] Begin resolving variables...\n");
    cmd->name = ksh_cmd_resolve_part(ksh, cmd->name);

    for (ksh_cmd_part_t* p = cmd->args; p; p = p->next)
        p->val = ksh_cmd_resolve_part(ksh, p->val);

    DEBUG("[D] Finished resolving vars!\n");
}

void ksh_exec_cmd(ksh_cmd_t* cmd, ksh_variable_list_t* env) {
    env = ksh_variable_list_merge(env, cmd->vars);

    // Calculate the estimated number of environment variables
    int n_envp;
    for (n_envp = 0; environ[n_envp] != NULL; n_envp++);

    ksh_variable_list_t* iter;

    for (iter = env; iter; n_envp++, iter = iter->next);

    n_envp++; // for the closing null pointer

    int ienvv = 0;
    char** envvars = _malloc(sizeof(char*) * n_envp, "ksh_exec_cmd$envvars");
    uint64_t hash;

    // Add environment variables from environ
    for (int i = 0; environ[i] != NULL; i++) {
        hash = fnv64_strn(environ[i], strchr(environ[i], '=') - environ[i]);

        for (iter = env; iter; iter = iter->next)
            if (env->key_hash == hash)
                break;

        if (!iter) {
            envvars[ienvv] = _malloc(strlen(environ[i]) + 1, "ksh_exec_cmd$envvar");
            strcpy(envvars[ienvv], environ[i]);
            ienvv++;
        }
    }

    for (iter = env; iter; ienvv++, iter = iter->next) {
        envvars[ienvv] = _malloc(strlen(iter->key) + strlen(iter->val) + 2, "ksh_exec_cmd$envvar");
        strcpy(envvars[ienvv], iter->key);
        strcat(envvars[ienvv], "=");
        strcat(envvars[ienvv], iter->val);
    }

    envvars[ienvv] = NULL;

    char** args = _malloc(sizeof(char*) * (cmd->nargs + 2), "ksh_exec_cmd$args");
    args[0] = cmd->exe;
    ksh_cmd_part_t* p = cmd->args;

    int i;
    for (i = 1; p; i++, p = p->next) {
        DEBUG("[D] Addarg %s\n", p->val);
        args[i] = p->val;
    }

    args[i] = NULL;

    DEBUG("[D] Executing %s with %d argument(s)\n", cmd->exe, cmd->nargs);
    int res = execve(cmd->exe, args, envvars);

    del(args);

    for (int i = 0; envvars[i] != NULL; i++)
        del(envvars[i]);

    del(envvars);
    exit(res);
    ksh_variable_list_light_free(&env);
}

bool ksh_split_equals_in_arg(const char* arg, char** outleft, char** outright) {
    char* chr = strchr(arg, '=');
    if (!chr)
        return false;

    *(chr++) = 0;

    size_t slen = strlen(chr);

    ksh_char_buffer_t* buff = ksh_char_buffer_init();

    char quote = 0;
    char ch = 0;
    char blastch = 0;
    char lastch = 0;
    for (int i = 0; i < slen; i++) {
        blastch = lastch;
        lastch = ch;
        ch = chr[i];

        if (ch == quote && lastch != '\\' && !(lastch == '\\' && blastch == '\\')) {
            quote = 0;
            continue;
        } else if (!quote && (ch == '"' || ch == '\'')) {
            quote = ch;
            continue;
        }

        if (ch == '\\' && lastch == '\\')
            continue;

        ksh_char_buffer_insert(buff, ch, -1);
    }

    *outright = ksh_char_buffer_make_string(buff, 0);
    *outleft = _malloc(strlen(arg), "eq_arg_left");
    strcpy(*outleft, arg);

    ksh_char_buffer_free(&buff);
    return true;
}
