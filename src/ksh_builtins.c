////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_builtins.c
 * @author Kiss Benedek
 * @brief Implementation of built-in commands
 */

#include "ksh_global.h"
#include "ksh_cmd.h"
#include "ksh_char_buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>

//! builtin command handler for exit
int ksh_builtin__exit(ksh_t* ksh, const ksh_cmd_t* cmd) {
    int exitcode = 0;
    if (cmd->nargs > 0)
        exitcode = atoi(cmd->args->val);

    ksh->request_quit = true;
    return exitcode;
}

//! internal helper function for changing working directory
bool cd_internal(char** newdir, const ksh_cmd_t* cmd) {
    *newdir = NULL;

    if (cmd->nargs == 0) {
        *newdir = getenv("HOME");

        if (chdir(*newdir) != 0)
            return false;
    } else {
        struct stat sb;
        *newdir = cmd->args->val;

        if (stat(*newdir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
            DEBUG("[D] Changing directory to: %s\n", *newdir);
            if (chdir(*newdir) != 0)
                return false;
        }
    }

    return true;
}

//! builtin command handler for cd
int ksh_builtin__cd(ksh_t* ksh, const ksh_cmd_t* cmd) {
    char* newdir;

    if (cd_internal(&newdir, cmd)) {
        del(ksh->dirstack->dir);
        ksh->dirstack->dir = get_mallocd_cwd();
        return 0;
    }

    printf("cd: no such file or directory: %s\n", cmd->args->val);
    return 1;
}

//! builtin command handler for pushd
int ksh_builtin__pushd(ksh_t* ksh, const ksh_cmd_t* cmd) {
    char* newdir;

    if (cd_internal(&newdir, cmd)) {
        ksh_directory_stack_t* current = ksh->dirstack;
        ksh->dirstack = new(ksh_directory_stack_t);
        ksh->dirstack->prev = current;
        ksh->dirstack->dir = get_mallocd_cwd();

        for (current = ksh->dirstack; current; current = current->prev)
            printf("%s ", current->dir);

        puts("");
        return 0;
    }

    printf("pushd: no such file or directory: %s\n", cmd->args->val);
    return 1;
}

//! builtin command handler for popd
int ksh_builtin__popd(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (!ksh->dirstack->prev) {
        printf("popd: directory stack empty\n");
        return 1;
    }

    ksh_directory_stack_t* next = ksh->dirstack->prev;
    del(ksh->dirstack->dir);
    del(ksh->dirstack);
    ksh->dirstack = next;

    struct stat sb;
    if (stat(ksh->dirstack->dir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
        if (chdir(ksh->dirstack->dir) == 0) {
            for (ksh_directory_stack_t* current = ksh->dirstack; current; current = current->prev)
                printf("%s ", current->dir);

            puts("");

            return 0;
        }
    }

    printf("popd: no such file or directory: %s\n", ksh->dirstack->dir);
    return 1;
}

//! builtin command handler for dirs
int ksh_builtin__dirs(ksh_t* ksh, const ksh_cmd_t* cmd) {
    for (ksh_directory_stack_t* current = ksh->dirstack; current; current = current->prev)
        printf("%s ", current->dir);

    puts("");
    return 0;
}

//! builtin command handler for alias
int ksh_builtin__alias(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (cmd->nargs == 0) {
        for (ksh_alias_list_t* current = ksh->aliases; current; current = current->next) {
            printf("%s='", current->command);

            for (char** parm = current->newparams; *parm; parm++) {
                printf("%s", *parm);

                if (*(parm + 1))
                    printf(" ");
            }

            printf("'\n");
        }

        return 0;
    }

    char* arg = cmd->args->val;
    char* chr;
    if ((chr = strchr(arg, '=')) == NULL) {
        printf("alias: invalid argument\n");
        return 1;
    }

    *chr = 0;
    chr++;

    size_t slen = strlen(chr);
    ksh_char_buffer_t* buff = ksh_char_buffer_init();
    ksh_cmd_t* tmp = ksh_cmd_init(KSH_CMDREL_STANDALONE);

    char outerquote = 0;

    if (!strchr(chr, '\'') && !strchr(chr, '"'))
        outerquote = 1;

    char lastquote = 0;
    char ch = 0;
    char blastch = 0;
    char lastch = 0;

    for (int i = 0; i < slen; i++) {
        blastch = lastch;
        lastch = ch;
        ch = chr[i];

        if (ch == outerquote && (lastch != '\\' || blastch == '\\'))
            break;
        else if (!outerquote) {
            if (ch == '"' || ch == '\'')
                outerquote = ch;

            continue;
        }

        if (ch == lastquote) {
            lastquote = 0;
            continue;
        } else if (!lastquote && (ch == '"' || ch == '\'')) {
            lastquote = ch;
            continue;
        }

        if (ch == '\\' && lastch == '\\')
            continue;

        if (ch == ' ' && !lastquote) {
            char* s = ksh_char_buffer_make_string(buff, 0);
            ksh_cmd_append_part(tmp, s, NULL);
            ksh_char_buffer_clear(buff);
            continue;
        }

        ksh_char_buffer_insert(buff, ch, -1);
    }

    if (buff->length > 0) {
        char* s = ksh_char_buffer_make_string(buff, 0);
        ksh_cmd_append_part(tmp, s, NULL);
    }

    if (tmp->name) {
        char* name = _malloc(strlen(arg), "alias_name");
        strcpy(name, arg);

        char** newparams = _malloc(sizeof(char*) * (tmp->nargs + 2), "alias_params");
        int i = 0;
        newparams[i] = _malloc(strlen(tmp->name) + 1, "alias_param");
        strcpy(newparams[i++], tmp->name);

        for (ksh_cmd_part_t* part = tmp->args; part; part = part->next) {
            newparams[i] = _malloc(strlen(part->val) + 1, "alias_param");
            strcpy(newparams[i++], part->val);
        }

        newparams[i] = NULL;

        ksh_add_alias(ksh, name, newparams);
    } else {
        printf("alias: invalid command format\n");
        ksh_char_buffer_free(&buff);
        ksh_cmd_destroy(&tmp);
        return 1;
    }

    ksh_char_buffer_free(&buff);
    ksh_cmd_destroy(&tmp);

    return 0;
}

//! builtin command handler for unalias
int ksh_builtin__unalias(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (cmd->nargs == 0) {
        printf("unalias: missing argument\n");
        return 1;
    }

    if (!ksh_remove_alias(ksh, cmd->args->val)) {
        printf("unalias: there is no alias named %s\n", cmd->args->val);
        return 1;
    }

    return 0;
}

//! builtin command handler for export
int ksh_builtin__export(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (cmd->nargs == 0) {
        for (ksh_variable_list_t* current = ksh->global_vars; current; current = current->next)
            printf("%s=%s\n", current->key, current->val);

        return 0;
    }

    char* kptr = NULL;
    char* vptr = NULL;
    if (!ksh_split_equals_in_arg(cmd->args->val, &kptr, &vptr)) {
        printf("export: invalid command format\n");
        return 1;
    }

    ksh_set_variable(ksh, kptr, vptr, false);
    del(kptr);

    return 0;
}

//! builtin command handler for set
int ksh_builtin__set(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (cmd->nargs == 0) {
        for (ksh_variable_list_t* current = ksh->local_vars; current; current = current->next)
            printf("%s=%s\n", current->key, current->val);

        return 0;
    }

    char* kptr = NULL;
    char* vptr = NULL;
    if (!ksh_split_equals_in_arg(cmd->args->val, &kptr, &vptr)) {
        printf("set: invalid command format\n");
        return 1;
    }

    ksh_set_variable(ksh, kptr, vptr, true);
    del(kptr);

    return 0;
}

//! builtin command handler for unset
int ksh_builtin__unset(ksh_t* ksh, const ksh_cmd_t* cmd) {
    if (cmd->nargs == 0) {
        printf("unset: missing argument\n");
        return 1;
    }

    ksh_unset_variable(ksh, cmd->args->val, true);
    ksh_unset_variable(ksh, cmd->args->val, false);
    return 0;
}
