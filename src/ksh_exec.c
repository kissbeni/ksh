////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_exec.c
 * @author Kiss Benedek
 * @brief Command execution engine implementation
 */

#include "ksh_global.h"
#include "ksh_exec.h"
#include "ksh_cmd.h"
#include "ksh_builtins.h"
#include "ksh_char_buffer.h"

#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

//! Names for builtin commands (same order as #builtin_funcs)
const char*         builtin_names[] = {
    "cd",
    "chdir",
    "exit",
    "pushd",
    "popd",
    "dirs",
    "alias",
    "unalias",
    "export",
    "set",
    "unset",
    NULL
};

//! Handlers for builtin commands (same order as #builtin_names)
const ksh_builtin_f builtin_funcs[] = {
    ksh_builtin__cd,
    ksh_builtin__cd,
    ksh_builtin__exit,
    ksh_builtin__pushd,
    ksh_builtin__popd,
    ksh_builtin__dirs,
    ksh_builtin__alias,
    ksh_builtin__unalias,
    ksh_builtin__export,
    ksh_builtin__set,
    ksh_builtin__unset,
    NULL
};

/**
 * @brief Finds a file with @p name
 *
 * Checks the file relative to the current
 * working directory.
 *
 * @return Thre relative path of the file or
 * NULL if the file does not exists
 */
char* ksh_resolve_path(const char* name) {
    if (unlikely(!name))
        return NULL;

    char* pth = _malloc(PATH_MAX, "resolved_cmd");

    if (name[0] == '/') {
        strncpy(pth, name, PATH_MAX);
        if (likely(access(pth, F_OK) != -1)) {
            DEBUG("[D] Found executable! (%s)\n", pth);
            return pth;
        }
    } else if (likely(getcwd(pth, PATH_MAX) != NULL)) {
        if (likely(pth[strlen(pth) - 1] != '/'))
            strncat(pth, "/", PATH_MAX);

        strncat(pth, name, PATH_MAX);
        char* pth2 = _malloc(PATH_MAX, "resolved_cmd_realpath");
        if (realpath(pth, pth2) != NULL) {
            del(pth);
            pth = pth2;

            if (likely(access(pth, F_OK) != -1)) {
                DEBUG("[D] Found file! (%s)\n", pth);
                return pth;
            }
        }

        del(pth2);
    }

    del(pth);
    return NULL;
}

char* ksh_resolve_command(const char* cmd) {
    if (unlikely(!cmd))
        return NULL;

    DEBUG("[D] Trying to resolve: '%s'\n", cmd);

    if (strchr(cmd, '/') != NULL) {
       return ksh_resolve_path(cmd);
    } else {
        char* path = getenv("PATH");

        ksh_char_buffer_t* buff = ksh_char_buffer_init();

        while (*path) {
            ksh_char_buffer_clear(buff);

            while (*path != ':' && *path) {
                ksh_char_buffer_insert(buff, *path, -1);
                path++;
            }

            char* q = ksh_char_buffer_make_string(buff, 0);
            if (q[0]) DEBUG("[D] Checking in %s\n", q);

            if (q[strlen(q) - 1] != '/')
                ksh_char_buffer_insert(buff, '/', -1);

            del(q);

            ksh_char_buffer_append_str(buff, cmd);
            q = ksh_char_buffer_make_string(buff, 0);

            if (access(q, F_OK) != -1) {
                DEBUG("[D] Found executable! (%s)\n", q);
                ksh_char_buffer_free(&buff);
                return q;
            }

            del(q);

            if (*path) path++;
        }

        ksh_char_buffer_free(&buff);
    }

    return NULL;
}

const char* ksh_cmdrel_to_str(ksh_cmdrel_e c) {
    switch (c) {
        case KSH_CMDREL_STANDALONE:
            return "STANDALONE";
        case KSH_CMDREL_AND:
            return "AND";
        case KSH_CMDREL_OR:
            return "OR";
        case KSH_CMDREL_DETACH:
            return "DETACH";
        case KSH_CMDREL_PIPE:
            return "PIPE";
        case KSH_CMDREL_FOUT:
            return "FOUT";
        case KSH_CMDREL_FOUTA:
            return "FOUTA";
        default:
            return "UNKNOWN";
    }
}

//! Helper function for adding the current buffer to the given command while removing trailing spaces
void ksh_exec_addpart(ksh_t* ksh, ksh_cmd_t* cmd, ksh_char_buffer_t* buff) {
    while (buff->length > 1 && ksh_char_buffer_at(buff, -1) == ' ')
        ksh_char_buffer_remove(buff, -1);

    char* q = ksh_char_buffer_make_string(buff, 0);

    if (q && q[0]) {
        ksh_cmd_append_part(cmd, q, ksh);
        ksh_char_buffer_clear(buff);
    } else del(q);
}

bool ksh_exec_s(ksh_t* ksh, const char* str) {
    assertp(ksh, "[!] ksh_exec_s: ksh cannot be null!");
    assertp(str, "[!] ksh_exec_s: str cannot be null!");

    ksh_cmd_t* cmd = ksh_cmd_init(KSH_CMDREL_STANDALONE);
    ksh_cmd_t* firstcmd = cmd;
    ksh->current_cmd = cmd;

    bool valid = false;

    size_t ptr = 0;
    char inString = 0;
    char prev = 0;
    bool midSubcmdString = false;
    bool hitComment = false;

    ksh_char_buffer_t* buff = ksh_char_buffer_init();

    while (str[ptr] && !hitComment) {
        if (!inString) {
            switch(str[ptr]) {
                case '#':
                    hitComment = true;
                    valid = true;
                    break;
                case '|':
                    ksh_exec_addpart(ksh, cmd, buff);

                    if (cmd->name) {
                        cmd->next = ksh_cmd_init(KSH_CMDREL_PIPE);
                        cmd = cmd->next;
                    } else if (!cmd->name && cmd->rel == KSH_CMDREL_PIPE) {
                        cmd->rel = KSH_CMDREL_OR;
                    } else {
                        printf("ksh: syntax error: invalid character '%c' at %ld\n", str[ptr], ptr);
                        goto quit;
                    }

                    ksh_char_buffer_clear(buff);
                    break;
                case '>':
                    ksh_exec_addpart(ksh, cmd, buff);

                    if (cmd->name) {
                        cmd->next = ksh_cmd_init(KSH_CMDREL_FOUT);
                        cmd = cmd->next;
                    } else if (!cmd->name && cmd->rel == KSH_CMDREL_FOUT) {
                        cmd->rel = KSH_CMDREL_FOUTA;
                    } else {
                        printf("ksh: syntax error: invalid character '%c' at %ld\n", str[ptr], ptr);
                        goto quit;
                    }

                    ksh_char_buffer_clear(buff);
                    break;
                case '&':
                    ksh_exec_addpart(ksh, cmd, buff);

                    if (cmd->name || cmd->vars) {
                        cmd->next = ksh_cmd_init(KSH_CMDREL_DETACH);
                        cmd = cmd->next;
                    } else if (!cmd->name && cmd->rel == KSH_CMDREL_DETACH) {
                        cmd->rel = KSH_CMDREL_AND;
                    } else {
                        printf("ksh: syntax error: invalid character '%c' at %ld\n", str[ptr], ptr);
                        goto quit;
                    }

                    ksh_char_buffer_clear(buff);
                    break;
                case ' ':
                    ksh_exec_addpart(ksh, cmd, buff);

                    if (buff->length > 0 && ksh_char_buffer_at(buff, -1) != ' ')
                        ksh_char_buffer_insert(buff, str[ptr], -1);
                    break;
                case '"':
                case '\'':
                    if (buff->length != 0) {
                        ksh_char_buffer_insert(buff, str[ptr], -1);
                        midSubcmdString = true;
                    }

                    inString = str[ptr];
                    break;
                case ';':
                    ksh_exec_addpart(ksh, cmd, buff);
                    ksh_char_buffer_clear(buff);
                    break;
                default:
                    ksh_char_buffer_insert(buff, str[ptr], -1);
                    break;
            }
        } else if (str[ptr] == inString && prev != '\\') {
            if (midSubcmdString) {
                ksh_char_buffer_insert(buff, str[ptr], -1);
                midSubcmdString = false;
            }

            inString = 0;
        } else {
            ksh_char_buffer_insert(buff, str[ptr], -1);
        }

        prev = str[ptr++];
    }

    valid |= cmd->name != NULL;

    ksh_exec_addpart(ksh, cmd, buff);

    ksh_cmd_t* piter = NULL;
    for (ksh_cmd_t* iter = firstcmd; iter; piter = iter, iter = iter->next) {
        if (iter->rel == KSH_CMDREL_FOUT || iter->rel == KSH_CMDREL_FOUTA) {
            if (!piter) {
                printf("ksh: there is nothing to pipe from\n");
                goto quit;
            }

            if (piter->rel == KSH_CMDREL_FOUT || piter->rel == KSH_CMDREL_FOUTA) {
                printf("ksh: don't know what to do with multiple pipe targets\n");
                goto quit;
            }

            piter->outfd = open(iter->name, O_WRONLY | (iter->rel == KSH_CMDREL_FOUTA ? O_APPEND : O_CREAT), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

            if (piter->outfd < 0) {
                printf("ksh: could not open %s for writing\n", iter->name);
                goto quit;
            }

            piter->nargs += iter->nargs;

            if (piter->args) {
                for (ksh_cmd_part_t* part = piter->args; part; part = part->next) {
                    if (!part->next) {
                        part->next = iter->args;
                        break;
                    }
                }
            } else
                piter->args = iter->args;

            piter->next = iter->next;

            del(iter->name);
        }

        if (piter && (piter->rel == KSH_CMDREL_FOUT || piter->rel == KSH_CMDREL_FOUTA))
            del(piter);
    }

    if (piter && (piter->rel == KSH_CMDREL_FOUT || piter->rel == KSH_CMDREL_FOUTA))
        del(piter);

    ksh_piop_t pipes;
    pipes.stdin.is_pipe = false;
    pipes.stdout.is_pipe = false;
    pipes.stderr.is_pipe = false;

    pipes.stdin.infd   = -1;
    pipes.stdout.outfd = -1;
    pipes.stderr.outfd = -1;

    pipes.stdin.outfd = STDIN_FILENO;
    pipes.stdout.infd = STDOUT_FILENO;
    pipes.stderr.infd = STDERR_FILENO;

    pipes.prev = NULL;

    ksh_exec(ksh, &pipes);

    for (ksh_cmd_t* iter = firstcmd; iter; iter = iter->next)
        if (iter->outfd != STDOUT_FILENO)
            close(iter->outfd);

    quit:

    ksh_char_buffer_free(&buff);
    ksh_cmd_destroy(&firstcmd);
    ksh->current_cmd = NULL;

    return valid;
}

//! Closes both ends of a #ksh_pipe_t
void kclose(ksh_pipe_t* pipe) {
    if (pipe->infd > STDERR_FILENO)
        close(pipe->infd);

    if (pipe->outfd > STDERR_FILENO)
        close(pipe->outfd);
}

void kcloseall(ksh_piop_t* pipes) {
    for (ksh_piop_t* iter = pipes; iter; iter = iter->prev) {
        kclose(&iter->stdin);
        kclose(&iter->stdout);
        kclose(&iter->stderr);
    }
}

void ksh_exec(ksh_t* ksh, ksh_piop_t* pipes) {
    assertp(ksh, "[!] ksh_exec: ksh cannot be null!");

    ksh_cmd_t* cmd = ksh->current_cmd;
    if (!cmd)
        return;

    if (!pipes->stdout.is_pipe)
        pipes->stdout.infd = cmd->outfd;

    DEBUG("[D] Command relation: %s\n", ksh_cmdrel_to_str(cmd->rel));

    if (cmd->rel == KSH_CMDREL_AND && ksh->last_exit_code != 0) {
        DEBUG(
            "[D] 'AND' constraint: skipping command %s because %d != 0\n",
            cmd->name,
            ksh->last_exit_code
        );
    } else if (cmd->rel == KSH_CMDREL_OR && ksh->last_exit_code == 0) {
        DEBUG(
            "[D] 'OR' constraint: skipping command %s because last command succeeded\n",
            cmd->name
        );
    } else if (cmd->name) {
        ksh_cmd_resolve_vars(ksh, cmd);
        ksh_cmd_debug_print(cmd);

        cmd->exe = ksh_resolve_command(cmd->name);
        const char* cmpp = cmd->exe ? cmd->exe : cmd->name;

        for (int i = 0; builtin_names[i]; i++)
            if (strcmp(cmpp, builtin_names[i]) == 0) {
                cmd->func = builtin_funcs[i];
                break;
            }

        if (cmd->func) {
            ksh->last_exit_code = cmd->func(ksh, cmd);
        } else {
            if (!cmd->exe) {
                printf("ksh: command not found: %s\n", cmd->name);
                ksh->last_exit_code = KSH_COMMAND_NOT_FOUND_EXITCODE;
            } else {
                struct stat st;
                if (stat(cmd->exe, &st) == 0 && (st.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH))) {
                    ksh_pipe_t* newpipe = NULL;

                    if (cmd->next && cmd->next->rel == KSH_CMDREL_PIPE) {
                        newpipe = new(ksh_pipe_t);
                        newpipe->is_pipe = true;
                        pipe(newpipe->raw);
                        pipes->stdout = *newpipe;
                    }

                    pid_t child = fork();

                    if (child == 0) {
                        DEBUG("[D] Running in forked process (we are %d)!\n", getpid());

                        DEBUG("[D] File descriptors: \n");
                        DEBUG("     - stdin: %d => %d\n", pipes->stdin.outfd, STDIN_FILENO);
                        DEBUG("     - stdout: %d => %d\n", pipes->stdout.infd, STDOUT_FILENO);
                        DEBUG("     - stderr: %d => %d\n", pipes->stderr.infd, STDERR_FILENO);

                        dup2(pipes->stdin.outfd, STDIN_FILENO);
                        dup2(pipes->stdout.infd, STDOUT_FILENO);
                        dup2(pipes->stderr.infd, STDERR_FILENO);

                        kcloseall(pipes);
                        ksh_exec_cmd(cmd, ksh->global_vars);
                    } else if (child < 0) {
                        printf("ksh: could not start child process\n");
                    } else {
                        ksh->fork_pid = child;
                        DEBUG("[D] Waiting for child process %d\n", child);

                        if (newpipe) {
                            ksh_piop_t* newpipes = new(ksh_piop_t);
                            memcpy(newpipes, pipes, sizeof(ksh_piop_t));
                            newpipes->stdin = *newpipe;
                            newpipes->stdout.is_pipe = false;
                            newpipes->stdout.outfd = STDOUT_FILENO;
                            newpipes->prev = pipes;
                            ksh->current_cmd = cmd->next;
                            ksh_exec(ksh, newpipes);
                            del(newpipes);
                        }

                        kcloseall(pipes);
                        waitpid(child, &ksh->last_exit_code, 0);
                        del(newpipe);

                        ksh->last_exit_code = WEXITSTATUS(ksh->last_exit_code);
                        char* int2str = _malloc(16, "exit_code_string");
                        sprintf(int2str, "%d", ksh->last_exit_code);
                        ksh_set_variable(ksh, "?", int2str, true);
                        DEBUG("[D] Exit code: %d\n", ksh->last_exit_code);

                        ksh->fork_pid = 0;
                    }
                } else
                    printf("zsh: permission denied: %s\n", cmd->exe);
            }
        }

        del(cmd->exe);

        if (ksh->request_quit) {
            DEBUG("[D] Requested quit, canceling further processing\n");
            return;
        }
    } else if (cmd->vars) {
        for (ksh_variable_list_t* list = cmd->vars; list; list = list->next) {
            char* val = _malloc(strlen(list->val) + 1, "inline_var_val");
            strcpy(val, list->val);
            ksh_set_variable(ksh, list->key, val, true);
        }

        ksh->last_exit_code = 0;
    }

    if (ksh->current_cmd) {
        ksh_cmd_t* next = ksh->current_cmd->next;
        ksh_cmdrel_e rel = next ? next->rel : KSH_CMDREL_STANDALONE;
        ksh->current_cmd = next;

        if (ksh->current_cmd && rel != KSH_CMDREL_PIPE)
            ksh_exec(ksh, pipes);
    }
}
