////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_prompt.c
 * @author Kiss Benedek
 * @brief Functions for user input and command history
 */

#include "ksh_prompt.h"
#include "ksh_char_buffer.h"
#include "ksh_exec.h"
#include "ksh_global.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#ifdef KSH_USE_LIB_READLINE
#  include <readline/readline.h>
#  include <readline/history.h>
#endif

static struct termios old;

void save_and_set_termios() {
    tcgetattr(0, &old);

    struct termios new;
    new = old;                /* make new settings same as old settings */
    new.c_lflag &= ~ICANON;   /* disable buffered i/o */
    new.c_lflag &= ~ECHO;     /* set no echo mode */

    tcsetattr(0, TCSANOW, &new);
}

void restore_termios() {
    tcsetattr(0, TCSANOW, &old);
}

char getch() {
    save_and_set_termios();
    char ch = getchar();
    restore_termios();
    return ch;
}

void ksh_prompt_init(ksh_t* ksh) {
    gethostname(ksh->prompt->hostname, KSH_PROMPT_HOST_MAXLEN);

    uid_t uid = getuid(), euid =  geteuid();

    struct passwd* pws;
    pws = getpwuid(uid);
    strncpy(ksh->prompt->user, pws->pw_name, KSH_PROMPT_USER_MAXLEN - 1);

    if (strlen(ksh->prompt->history_file) == 0) {
        strncpy(ksh->prompt->history_file, pws->pw_dir, PATH_MAX - 1);
        strncat(ksh->prompt->history_file, "/.ksh_history", PATH_MAX - strlen(ksh->prompt->history_file));
    }

    #ifdef KSH_USE_LIB_READLINE
    ksh->prompt->last_hist_id = 0;

    rl_ignore_completion_duplicates = 1;
    // TODO: register autocomplete handler
    // rl_attempted_completion_function = ...;
    if (access(ksh->prompt->history_file, F_OK) != -1)
        read_history(ksh->prompt->history_file);

    HIST_ENTRY** entry = history_list();

    while (entry && *(entry++))
        ksh->prompt->last_hist_id++;

    #else
    DEBUG("[D] Using history file: %s\n", ksh->prompt->history_file);

    ksh->prompt->history = NULL;

    // Load history
    FILE* fp = fopen(ksh->prompt->history_file, "r");
    if (fp != NULL) {
        char* line = NULL;
        size_t len = 0;
        ssize_t read;

        while ((read = getline(&line, &len, fp)) != -1) {
            line[read - 1] = 0;
            ksh_prompt_append_history(ksh->prompt, line, true, true);
        }

        free(line); // do not use our custom free here!

        fclose(fp);
    }
    #endif

    if (uid <= 0 || uid != euid)
        ksh->prompt->prompt_char = '#';
    else
        ksh->prompt->prompt_char = '$';
}

void ksh_prompt_destroy(ksh_prompt_t* prompt) {
    #ifdef KSH_USE_LIB_READLINE
        write_history(prompt->history_file);
    #else
        ksh_prompt_history_save(prompt);
        ksh_prompt_history_free(&prompt->history);
    #endif
}

#ifdef KSH_USE_LIB_READLINE
    void ksh_prompt_append_history(ksh_prompt_t* prompt, char* line) {
        char* prevline = prompt->last_hist_id > 0 ? history_get(prompt->last_hist_id)->line : "";
            if (strcmp(prevline, line) != 0) {
                add_history(line);
                prompt->last_hist_id++;
            }
    }

    void ksh_prompt_make_prompt(char* outbuf, ksh_t* ksh) {
        char cwd[PATH_MAX];

        strncpy(cwd, ksh->dirstack->dir, PATH_MAX - 1);
        size_t pos = strlen(cwd);

        if (pos > 1 && strchr(cwd + 1, '/')) {
            while (pos > 0 && cwd[pos - 1] != '/') pos--;
            strncpy(cwd, &cwd[pos], PATH_MAX - 1);
        }

        sprintf(
            outbuf,
            "[%s@%s %s]%c ",
            ksh->prompt->user,
            ksh->prompt->hostname,
            cwd,
            ksh->prompt->prompt_char
        );
    }

    void ksh_promt_read_loop(ksh_t* ksh) {
        char* line;

        char* prompt = _malloc(
            strlen("[@ ]? ") +
            KSH_PROMPT_USER_MAXLEN +
            KSH_PROMPT_HOST_MAXLEN +
            PATH_MAX +
            1,
            "Prompt string"
        );

        ksh_prompt_make_prompt(prompt, ksh);

        while ((line = readline(prompt)) != NULL) {
            if (ksh_exec_s(ksh, line))
                ksh_prompt_append_history(ksh->prompt, line);

            free(line);
            ksh_prompt_make_prompt(prompt, ksh);

            if (ksh->request_quit)
                break;
        }

        del(prompt);
    }
#endif
