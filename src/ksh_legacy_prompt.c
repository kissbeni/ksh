////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_legacy_prompt.c
 * @author Kiss Benedek
 * @brief Legacy functions for user input and command history
 */

#include "ksh_global.h"
#include "ksh_prompt.h"
#include "ksh_char_buffer.h"
#include "ksh_exec.h"

#include <string.h>

#ifndef KSH_USE_LIB_READLINE

    ksh_history_t* ksh_prompt_history_init() {
        ksh_history_t* history = new(ksh_history_t);
        history->next = NULL;
        history->prev = NULL;
        history->line = NULL;
        history->id = 0;
        return history;
    }

    void ksh_prompt_history_free(ksh_history_t** history) {
        assertp(history, "[!] ksh_prompt_history_free: history cannot be null!");

        if (*history) {
            ksh_history_t *h = *history, *h2;
            while (h->prev)
                h = h->prev;

            while (h) {
                h2 = h->next;
                del(h->line);
                del(h);
                h = h2;
            }
        }

        *history = NULL;
    }

    void ksh_prompt_append_history(ksh_prompt_t* prompt, char* value, bool copy, bool fromfile) {
        assertp(prompt, "[!] ksh_prompt_append_history: prompt cannot be null!");
        assertp(value, "[!] ksh_prompt_append_history: Appending a null to history is not allowed!");

        if (strlen(value) == 0) {
            if (!copy)
                del(value);

            return;
        }

        DEBUG("[D] History append: %s [copy: %s] [fromfile: %s]\n", value, copy ? "true" : "false", fromfile ? "true" : "false");

        if (!prompt->history)
            prompt->history = ksh_prompt_history_init();

        while (prompt->history->next)
            prompt->history = prompt->history->next;

        if (prompt->history->line) {
            // This will prevent duplicate entries in the history list
            if (strcmp(prompt->history->line, value) == 0) {
                if (!copy)
                    del(value);

                return;
            }

            ksh_history_t* tmp = prompt->history;
            tmp->next = ksh_prompt_history_init();
            tmp->next->prev = tmp;
            prompt->history = tmp->next;

            for (; tmp; tmp = tmp->prev)
                if (tmp->next)
                    tmp->id = (tmp->next->id + 1) | (tmp->id & KSH_HISTFROMFILE_FLAG);
        }

        if (fromfile)
            prompt->history->id |= KSH_HISTFROMFILE_FLAG;

        if (copy) {
            prompt->history->line = _malloc(strlen(value) + 1, "history->line");
            strcpy(prompt->history->line, value);
        } else {
            prompt->history->line = value;
        }
    }

    ksh_history_t* ksh_prompt_history_get(ksh_prompt_t* prompt, int32_t index) {
        if (unlikely(!prompt->history || index < 0))
            return NULL;

        ksh_history_t* tmp;

        if (KSH_HISTID(prompt->history) <= index) {
            for (tmp = prompt->history; tmp; tmp = tmp->prev) {
                if (KSH_HISTID(tmp) == index) {
                    prompt->history = tmp;
                    return tmp;
                }
            }
        } else {
            for (tmp = prompt->history->next; tmp; tmp = tmp->next) {
                if (KSH_HISTID(tmp) == index) {
                    prompt->history = tmp;
                    return tmp;
                }
            }
        }

        return NULL;
    }

    void ksh_prompt_history_save(ksh_prompt_t* prompt) {
        if (!prompt->history)
            return;

        uint32_t n_saved = 0;

        FILE* fp = fopen(prompt->history_file, "at");
        if (fp != NULL) {
            while (prompt->history->prev)
                prompt->history = prompt->history->prev;

            for (ksh_history_t* hist = prompt->history; hist; hist = hist->next) {
                // Most of the times we will have less entries in memory
                // than in file, so unlikely will make saving a bit faster
                if (unlikely(!(hist->id & KSH_HISTFROMFILE_FLAG))) {
                    fprintf(fp, "%s\n", hist->line);
                    n_saved++;
                }
            }

            fclose(fp);
        }

        DEBUG("[D] Saved %u line(s) of history to %s\n", n_saved, prompt->history_file);
    }

    void ksh_prompt_print(const ksh_t* ksh) {
        char cwd[PATH_MAX];
        strncpy(cwd, ksh->dirstack->dir, PATH_MAX - 1);
        size_t pos = strlen(cwd);
        while (pos > 0 && cwd[pos - 1] != '/') pos--;
        strncpy(cwd, &cwd[pos], PATH_MAX - 1);

        printf(
            "[%s@%s %s]%c ",
            ksh->prompt->user,
            ksh->prompt->hostname,
            cwd,
            ksh->prompt->prompt_char
        );
    }

    //! Reads an ANSI escape sequence from stdin to @p res
    void ksh_prompt_read_cs_sequence(ksh_cs_params_t* res) {
        int param_indx = 0;
        bool can_process_integer = true;
        char rch;
        while ((rch = getch()) >= 0x30 && rch <= 0x3F) {
            if (rch <= 0x39) {
                res->separator_byte = 0;

                if (can_process_integer) {
                    res->int_params[param_indx] *= 10;
                    res->int_params[param_indx] += rch - 0x30;
                }
            } else {
                if (res->separator_byte == 0) {
                    res->separator_byte = rch;
                    param_indx++;

                    if (param_indx == 5) {
                        DEBUG("[D] Reached max storeable control sequence size\n");
                        can_process_integer = false;
                    }
                }
            }
        }

        while (rch >= 0x20 && rch <= 0x2F)
            rch = getch();

        res->final_byte = rch;
    }

    bool ksh_prompt_read_and_exec(ksh_t* ksh) {
        save_and_set_termios();
        ksh_prompt_t* prompt = ksh->prompt;
        ksh_char_buffer_t* buff = ksh_char_buffer_init();

        char rch;                       // The last read character
        bool store_ch;                  // Do we store the read character?
        int32_t char_index = 0;         // Where is the character position?
        int32_t cursor_index = 0;       // Where is the cursor?
        int32_t chars_entered = 0;      // Number of characters (bytes) entered
        int32_t utf8chars_entered = 0;  // Number of UTF8 characters read
        int32_t history_indx = -1;      // Index of the current history entry
        char* saved_line = NULL;        // Pointer to the saved line
        bool in_history = false;        // Are we browsing the history?
        ksh_history_t* h;               // The current history entry that is printed
        ksh_cs_params_t csi;            // Variable for storing escape sequence info

        do {
            store_ch = false;
            switch (rch = getchar()) {
                // Terminator characters
                case ASCII_ETX:
                case ASCII_EOT:
                case EOF:
                case ASCII_CAN:
                    del(saved_line);
                    ksh_char_buffer_free(&buff);
                    restore_termios();

                    if (rch == ASCII_CAN) {
                        puts("");
                        return true;
                    }
                    return false;

                // Escape character
                case ASCII_ESC:
                    {
                        rch = getch();
                        if (rch == '[') {
                            ksh_prompt_read_cs_sequence(&csi);

                            switch (csi.final_byte) {
                                case ANSICB_CUU: // Up
                                    history_indx++;
                                    h = ksh_prompt_history_get(prompt, history_indx);
                                    if (h && h->line) {
                                        if (cursor_index < utf8chars_entered)
                                            printf("\x1b[%dC", utf8chars_entered - cursor_index);

                                        for (int i = 0; i < utf8chars_entered; i++)
                                            printf("\x1b[1D \b");

                                        if (!in_history) {
                                            del(saved_line);

                                            saved_line = ksh_char_buffer_make_string(buff, 0);

                                            in_history = true;
                                        }

                                        printf("%s", h->line);
                                        ksh_char_buffer_set_string(buff, h->line);

                                        DEBUG("Bef: %d %d %d %d\n", char_index, chars_entered, utf8chars_entered, cursor_index);
                                        char_index = chars_entered = strlen(h->line);
                                        utf8chars_entered = cursor_index = utf8len(h->line);
                                        DEBUG("Aft: %d %d %d %d\n", char_index, chars_entered, utf8chars_entered, cursor_index);
                                    } else
                                        history_indx--;

                                    break;
                                case ANSICB_CUD: // Down
                                    history_indx--;
                                    if (history_indx <= -1) {
                                        history_indx = -1;

                                        if (cursor_index < utf8chars_entered)
                                            printf("\x1b[%dC", utf8chars_entered - cursor_index);

                                        for (int i = 0; i < utf8chars_entered; i++)
                                            printf("\x1b[1D \b");

                                        if (saved_line) {
                                            printf("%s", saved_line);

                                            chars_entered = char_index = strlen(saved_line);
                                            utf8chars_entered = cursor_index = utf8len(saved_line);
                                            ksh_char_buffer_set_string(buff, saved_line);
                                        } else {
                                            chars_entered = 0;
                                            char_index = 0;
                                            utf8chars_entered = cursor_index = 0;
                                        }

                                        in_history = false;
                                    } else {
                                        h = ksh_prompt_history_get(prompt, history_indx);
                                        if (h && h->line) {
                                            if (cursor_index < utf8chars_entered)
                                                printf("\x1b[%dC", utf8chars_entered - cursor_index);

                                            for (int i = 0; i < utf8chars_entered; i++)
                                                printf("\x1b[1D \b");

                                            if (!in_history) {
                                                del(saved_line);

                                                saved_line = ksh_char_buffer_make_string(buff, 0);
                                                in_history = true;
                                            }

                                            printf("%s", h->line);
                                            ksh_char_buffer_set_string(buff, h->line);

                                            chars_entered = strlen(h->line);
                                            char_index = chars_entered;
                                            utf8chars_entered = cursor_index = utf8len(h->line);
                                        }
                                    }
                                    break;
                                case ANSICB_CUF: // Right
                                    if (cursor_index < utf8chars_entered) {
                                        char_index++;
                                        cursor_index++;
                                        printf("\x1b[1C");
                                    }
                                    break;
                                case ANSICB_CUB: // Left
                                    if (cursor_index > 0) {
                                        char_index--;
                                        cursor_index--;
                                        printf("\x1b[1D");
                                    }
                                    break;
                                case ANSICB_CUE: // End
                                    if (cursor_index < utf8chars_entered) {
                                        int inc = utf8chars_entered - cursor_index;
                                        char_index += inc;
                                        cursor_index += inc;
                                        printf("\x1b[%dC", inc);
                                    }
                                    break;
                                case ANSICB_CUH: // Home
                                    if (cursor_index > 0) {
                                        int dec = cursor_index;
                                        char_index = 0;
                                        cursor_index = 0;
                                        printf("\x1b[%dD", dec);
                                    }
                                    break;
                                default:
                                    DEBUG("[D] Unhandled ANSI CB: %02x\n", csi.final_byte);
                            }
                        } else {
                            int32_t insert_size = ksh_char_buffer_insert(buff, 0x1B, char_index);
                            char_index += insert_size;
                            chars_entered += insert_size;
                            store_ch = true;
                        }

                        break;
                    }
                // Backspace
                case ASCII_DEL:
                    {
                        if (utf8chars_entered > 0 && char_index > 0) {
                            int32_t nrm = ksh_char_buffer_remove(buff, char_index - 1);
                            DEBUG("ksh_char_buffer_remove returned %d\n", nrm);
                            char_index -= nrm;
                            chars_entered -= nrm;
                            cursor_index--;
                            utf8chars_entered--;

                            char* tmp = ksh_char_buffer_make_string(buff, 0);
                            DEBUG("[D] buffer: %s\n", tmp);
                            del(tmp);

                            tmp = ksh_char_buffer_make_string(buff, char_index);

                            if (tmp)
                                printf("\b%s \x1b[%ldD", tmp, utf8len(tmp) + 1);
                            else {
                                DEBUG("[D] tmp is null :(\n");
                                printf("\b \x1b[1D");
                            }

                            del(tmp);
                        }

                        break;
                    }
                case ASCII_TAB:
                    DEBUG("[D] Autocomplete request (not implemented feature)\n");
                    break;
                case ASCII_LF:
                    break;
                default:
                    store_ch = true;
                    break;
            }

            if (store_ch) {
                printf("%c", rch);

                if (char_index != chars_entered && (is_utf8_s(rch) || is_utf8_a(rch))) {
                    char* tmp = ksh_char_buffer_make_string(buff, char_index);

                    if (likely(tmp)) {
                        /*while (!is_utf8_m(tmp[0]))
                            tmp++;*/

                        printf("%s", tmp);
                        printf("\x1b[%ldD", utf8len(tmp));
                    }

                    del(tmp);
                }

                int32_t insert_size = ksh_char_buffer_insert(buff, rch, char_index);
                char_index += insert_size;
                chars_entered += insert_size;

                // Do not increment on non-master utf-8 datapoints
                if (is_utf8_m(rch)) {
                    cursor_index++;
                    utf8chars_entered++;
                }
            }
        } while (rch != 0x0A);

        del(saved_line);

        puts("");

        char* str = ksh_char_buffer_make_string(buff, 0);
        ksh_exec_s(ksh, str);
        ksh_prompt_append_history(prompt, str, false, false);

        ksh_char_buffer_free(&buff);
        restore_termios();
        return true;
    }

    void ksh_promt_read_loop(ksh_t* ksh) {
        do {
            if (ksh->request_quit)
                break;

            ksh_prompt_print(ksh);
        } while  (ksh_prompt_read_and_exec(ksh));
    }

#endif
