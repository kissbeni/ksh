////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                    //
//                                                                                //
// Copyright (c) 2018 Kiss Benedek                                                //
//                                                                                //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
//                                                                                //
// The above copyright notice and this permission notice shall be included in all //
// copies or substantial portions of the Software.                                //
//                                                                                //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  //
// SOFTWARE.                                                                      //
////////////////////////////////////////////////////////////////////////////////////

/**
 * @file ksh_global.c
 * @author Kiss Benedek
 * @brief Implementation of global functions
 */

#include "ksh_global.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>

#ifdef KSH_MEMORY_DEBUGGING
    //! This value indacates the validity of a #ksh_debug_memory_block_t
    #define MEM_EXPECTED_CANARY 0xDEADBEEF

    static size_t ksh_b_allocated = 0;
    static size_t ksh_b_freed = 0;
    static unsigned int ksh_n_mallocs = 0;
    static unsigned int ksh_n_frees = 0;
    static unsigned int ksh_n_reallocs = 0;

    //! Extra information about memory chunks on the heap
    typedef struct {
        uint32_t canary;    //!< Value for checking validity @see MEM_EXPECTED_CANARY
        size_t   size;      //!< Size that was requested (not the heap chunk size!)
        char     name[64];  //!< Name of this block
    } ksh_debug_memory_block_t;
#endif

void* _malloc(size_t size, const char* debug_name) {
    #ifdef KSH_MEMORY_DEBUGGING
        size += sizeof(ksh_debug_memory_block_t);

        ksh_debug_memory_block_t* res = malloc(size);
        assertf(res, "[!] Could not allocate %ld bytes of memory for %s!\n", size, debug_name);

        ksh_n_mallocs++;
        ksh_b_allocated += size;

        res->canary = MEM_EXPECTED_CANARY;
        res->size = size - sizeof(ksh_debug_memory_block_t);
        strncpy(res->name, debug_name, 64);

        const char* bintype = "large";

        if (size <= 80) bintype = "fast";
        else if (size <= 512) bintype = "small";

        size_t chunksize = (res ? *(((size_t*)res) - 1) : 0) & ~((size_t)7);

        MDEBUG("[D] malloc(%ld) returned with %p for object %s (possibly a %s bin) chunksize=%ld\n", size, res, debug_name, bintype, chunksize);

        return res + 1;
    #else
        void* res = malloc(size);
        if (unlikely(!res)) {
            puts("ksh: memory allocation failed");
            abort();
        }

        return res;
    #endif
}

void _free(void** p, const char* debug_name) {
    #ifdef KSH_MEMORY_DEBUGGING
        assertp(p, "[!] Tried to call _free with a null argument");

        if (unlikely(!*p))
            return;

        ksh_debug_memory_block_t* chk = *p;
        chk--;

        if (unlikely(chk->canary != MEM_EXPECTED_CANARY)) {
            printf("[!] Got unexpected canary [0x%08x] for free(%s [%p])\n", chk->canary, debug_name, *p);
            *p = NULL;
            return;
        }

        MDEBUG("[D] free(%s [%p]) releasing [%s] containing %ld bytes\n", debug_name, *p, chk->name, chk->size);

        ksh_b_freed += chk->size + sizeof(ksh_debug_memory_block_t);

        memset(chk, 0xFF, chk->size + sizeof(ksh_debug_memory_block_t));
        free(chk);
        ksh_n_frees++;
    #else
        if (unlikely(!p || !*p))
            return;

        free(*p);
    #endif

    *p = NULL;
}

void* _realloc(void* p, size_t newsize) {
    #ifdef KSH_MEMORY_DEBUGGING
        assertp(p, "[!] Tried to call _free with a null argument");

        ksh_debug_memory_block_t* chk = p;
        chk--;

        if (unlikely(chk->canary != MEM_EXPECTED_CANARY)) {
            printf("[!] Got unexpected canary for realloc(%p)\n", p);
            return p;
        }

        // This will save time, if we have memory blocks that are made smaller than larger
        if (unlikely(chk->size >= newsize))
            return p;

        MDEBUG("[D] realloc(%p) resizing %s from %ld to %ld bytes\n", p, chk->name, chk->size, newsize);

        ksh_b_allocated -= chk->size;
        ksh_b_allocated += newsize;

        chk->size = newsize;

        ksh_debug_memory_block_t* res = realloc(chk, chk->size + sizeof(ksh_debug_memory_block_t));
        assertf(res, "[!] Could not realloc %s\n", chk->name);
        ksh_n_reallocs++;
        return res + 1;
    #else
        void* res = realloc(p, newsize);
        if (unlikely(!res)) {
            puts("ksh: memory reallocation failed");
            abort();
        }

        return res;
    #endif
}

void ksh_print_mem_statistics() {
    #ifdef KSH_MEMORY_DEBUGGING
        DEBUG("[D] Memory management statistics:\n");
        DEBUG("    - malloc calls   : %u\n",  ksh_n_mallocs);
        DEBUG("    - free calls     : %u\n",  ksh_n_frees);
        DEBUG("    - realloc calls  : %u\n",  ksh_n_reallocs);
        DEBUG("    - total allocated: %lu\n", ksh_b_allocated);
        DEBUG("    - total freed    : %lu\n", ksh_b_freed);

        if (ksh_n_mallocs > ksh_n_frees)
            printf("[!] Leaking %lu bytes of memory\n", ksh_b_allocated - ksh_b_freed);
    #endif
}

void ksh_debug_pipe_puts(const char* s) {
    mkfifo("/tmp/ksh_debug", 0666);
    int fd = open("/tmp/ksh_debug", O_WRONLY);
    write(fd, s, strlen(s));
    write(fd, "\n", 1);
    fsync(fd);
    close(fd);
}

void ksh_debug_pipe_printf(const char* format, ...) {
    char buffer[65536];
    va_list args;
    va_start(args, format);
    vsnprintf(buffer, 65535, format, args);
    va_end(args);

    mkfifo("/tmp/ksh_debug", 0666);
    int fd = open("/tmp/ksh_debug", O_WRONLY);
    write(fd, buffer, strlen(buffer));
    fsync(fd);
    close(fd);
}

bool ksh_add_alias(ksh_t* ksh, char* name, char** newcmd) {
    DEBUG("[D] Adding alias as %s\n", name);
    uint64_t hash = fnv64_str(name);

    for (ksh_alias_list_t* item = ksh->aliases; item; item = item->next)
        if (item->command_hash == hash) {
            del(name);

            for (char** a = item->newparams; a && *a; a++)
                del(*a);

            del(item->newparams);
            item->newparams = newcmd;
            return false;
        }

    ksh_alias_list_t* newalias = new(ksh_alias_list_t);
    newalias->command = name;
    newalias->newparams = newcmd;
    newalias->command_hash = hash;
    newalias->next = NULL;

    if (ksh->aliases)
        ksh->aliases->next = newalias;
    else
        ksh->aliases = newalias;

    return true;
}

bool ksh_remove_alias(ksh_t* ksh, const char* name) {
    uint64_t hash = fnv64_str(name);
    for (ksh_alias_list_t *item = ksh->aliases, *prev = NULL; item; prev = item, item = item->next)
        if (item->command_hash == hash) {
            if (prev)
                prev->next = item->next;

            for (char** a = item->newparams; a && *a; a++)
                del(*a);

            del(item->newparams);
            del(item->command);

            if (item == ksh->aliases) {
                del(ksh->aliases);
            } else {
                del(item);
            }

            return true;
        }

    return false;
}

char** ksh_resolve_alias(ksh_t* ksh, const char* name) {
    uint64_t hash = fnv64_str(name);
    for (ksh_alias_list_t* item = ksh->aliases; item; item = item->next)
        if (item->command_hash == hash)
            return item->newparams;

    return NULL;
}

void ksh_free_alias_list(ksh_t* ksh) {
    while (ksh->aliases) {
        ksh_alias_list_t* next = ksh->aliases->next;
        del(ksh->aliases->command);

        for (char** a = ksh->aliases->newparams; a && *a; a++)
            del(*a);

        del(ksh->aliases->newparams);
        del(ksh->aliases);
        ksh->aliases = next;
    }
}

void ksh_free_directory_stack(ksh_t* ksh) {
    ksh_directory_stack_t* next;

    for (ksh_directory_stack_t* st = ksh->dirstack; st; st = next) {
        next = st->prev;
        del(st->dir);
        del(st);
    }
}

char* get_mallocd_cwd() {
    char* p = _malloc(PATH_MAX, "get_mallocd_cwd");
    if (!getcwd(p, PATH_MAX))
        strcpy(p, "???");

    return p;
}

uint64_t fnv64_str(const char* str) {
    return fnv64_strn(str, SIZE_MAX);
}

uint64_t fnv64_strn(const char* str, size_t length) {
    if (length == SIZE_MAX)
        length = strlen(str);

    uint8_t* s = (uint8_t*)str;
    uint64_t hash = FNV64_INIT;

    while (length-- > 0) {
        hash ^= (uint64_t)*s++;
        hash *= FNV64_PRIME;
    }

    return hash;
}
