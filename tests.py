#!/bin/python3

# ksh testing framework main program

import time
import subprocess
import re

NTESTS = 6 # Number of total tests (TODO: dynamic value?)
TESTS_OK = 0

KSH_PROMPT_REGEX = re.compile(r'\[\S+@\S+ (.*?)\][$#]');

def ksh_newproc(args=[]):
    args = ["./ksh"] + args
    return subprocess.Popen(args, bufsize=1, universal_newlines=True, stdout = subprocess.PIPE, stdin = subprocess.PIPE )

def ksh_shell_exec(stdin):
    proc = ksh_newproc()
    #buf = []
    #match = None
    #proc.stdin.write(stdin)
    #proc.stdin.close()
    #while True:
    #    line = proc.stdout.readline()

    #    if len(line) == 0:
    #        break

    #    line = line.strip()
    #    buf.append(line)

    #proc.wait()
    cr = proc.communicate(stdin)

    if cr[1]:
        print_error(cr[1])

    return (cr[0].split('\n'), proc.returncode)

def print_info(s):
    global TESTS_OK
    print("[%2d/%2d] %s" % (TESTS_OK + 1, NTESTS, s))

def print_error(s):
    global TESTS_OK
    print("[%2d/%2d] \x1b[91m%s\x1b[39m" % (TESTS_OK + 1, NTESTS, s))

def test_ok():
    global TESTS_OK
    print("[%2d/%2d] \x1b[32mOk!\x1b[39m" % (TESTS_OK + 1, NTESTS))

    if TESTS_OK >= NTESTS:
        print("Completed all %d test of %d!" % (TESTS_OK + 1, NTESTS))

    TESTS_OK += 1

# buffer overflow tests:

SMALL_DATA = ("A" * 255) + "B"
MEDIUM_DATA = ("A" * 4066) + "B"
LARGE_DATA = ("A" * 65535) + "B"

for data in [SMALL_DATA, MEDIUM_DATA, LARGE_DATA]:
    print_info("Sending echo command with %d characters as argument" % len(data))
    rdata, excode = ksh_shell_exec("echo " + data + "\n")

    if excode != 0:
        print_error("Test failed! Exit code was %d (expecting 0)" % excode)
        exit(1)
    else:
        test_ok()

    print_info("Sending junk command of %d characters" % len(data))
    rdata, excode = ksh_shell_exec(data + "\n")

    if excode != 127:
        print_error("Test failed! Exit code was %d (expecting 0)" % excode)
        exit(1)
    else:
        test_ok()



