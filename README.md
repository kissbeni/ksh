# ksh

[![pipeline status](https://gitlab.com/kissbeni/ksh/badges/master/pipeline.svg)](https://gitlab.com/kissbeni/ksh/commits/master)

ksh (aka kissbeni's shell), is a minimal UNIX shell with bash compatibility.

### Progress

What's done?
 - Command history loading/saving
 - Generic command parsing/execution
 - __AND__ and __OR__ command relations
 - Command executable resolving
 - Prompt input
 - Piping commands to files
 - Built-in commands (all listed below)
 - Command-line arguments (except `-c`)
 - Variable replacing with `$NAME` and `${NAME}` variants
 - UTF-8 editing (works with libreadline)

What'll be implemented?
 - Detatching programs
 - Autocomplete

### Built-in commands

Command | Description
--------|------------
`help` | List built-in commands
`cd [directory]` | Change directory or jump to home
`chdir [directory]` | Same as `cd`
`exit [exitcode]` | Exit with specified status code or 0 if not specified
`pushd [directory]` | Push a directory (or `$HOME`) the directory stack then change to it
`popd` | Pops a directory from the directory stack while changing to it
`dirs` | Prints the current directory stack
`alias [<alias name>=<replacement>]` | Create a command alias or list aliases
`unalias <alias name>` | Remove an alias
`export [<name>=<value>]` | Create a global variable
`set [<name>=<value>]` | Create a local variable
`unset <name>` | Remove a variable


### Command-line arguments

Switch | Description
-------|------------
`-h,--help` | Show help then quit
`-c COMAND` | Run command then exit
`-H HISTFILE` | Specify the history file
`-i HISTFILE` | Import history from the specified file
`-v,--verbose` | Be verbose
`-V,--memdebug` | Debug memory management

### Documentation

The program is documented via [doxygen](http://www.doxygen.org/). Use `doxygen doxygen.cfg` to generate documentation to `./doxygen/html/`.

### Building

If you have the tools (cmake, make, gcc) and the readline library (you don't need that for the legacy prompt); type `cmake . && make` and you should be done :)

*In order to compile with the original prompt logic, set the `USE_READLINE` cmake variable to YES.*

## License

```
MIT License

Copyright (c) 2018 Kiss Benedek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
